#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config
import poppytools.utils.min_jerk as min_jerk

import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config
import math

# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()


lfoot = pressure.FootPressure('/dev/poppy_lfoot')
rfoot = pressure.FootPressure('/dev/poppy_rfoot')

lfoot.start()
rfoot.start()




poppy.power_up()

# poppy.l_ankle_x.pid = (0.3, 0, 0)
# poppy.r_ankle_x.pid = (0.3, 0, 0)

poppy.l_shoulder_x.goal_position = 70.0
poppy.r_shoulder_x.goal_position =- 70.0

poppy.l_shoulder_y.goal_position =- 5.0
poppy.r_shoulder_y.goal_position =- 5.0


c, t1, t2, t3, t4, t5 = kinematics_dev.get_para( 0.025 )#+ 0.075)

print c, np.degrees(t1), np.degrees(t2), np.degrees(t3 - np.pi / 2.0), np.degrees(t4 - np.pi / 2.0), np.degrees(t5)

lsum = []
rsum = []

print "init"
for i in range(100):

    lsum.append(lfoot.pressor_values[1] + lfoot.pressor_values[2] + lfoot.pressor_values[3] + lfoot.pressor_values[4])
    rsum.append(rfoot.pressor_values[1] + rfoot.pressor_values[2] + rfoot.pressor_values[3] + rfoot.pressor_values[4])
    time.sleep(0.05)


lsum = np.nansum(lsum) / (np.count_nonzero(~np.isnan(lsum)))
rsum = np.nansum(rsum) / (np.count_nonzero(~np.isnan(rsum)))



print "init done ", lsum, rsum

# for i in range(500):
#     print (lfoot.pressor_values[1] + lfoot.pressor_values[2] + lfoot.pressor_values[3] + lfoot.pressor_values[4]) / lsum, (rfoot.pressor_values[1] + rfoot.pressor_values[2] + rfoot.pressor_values[3] + rfoot.pressor_values[4]) / rsum, ((lfoot.pressor_values[1] + lfoot.pressor_values[2]) - (lfoot.pressor_values[3] + lfoot.pressor_values[4])) / lsum, ((rfoot.pressor_values[1] + rfoot.pressor_values[2]) - (rfoot.pressor_values[3] + rfoot.pressor_values[4])) / rsum, ((lfoot.pressor_values[1] + lfoot.pressor_values[2]) - (lfoot.pressor_values[3] + lfoot.pressor_values[4])) / lsum + ((rfoot.pressor_values[1] + rfoot.pressor_values[2]) - (rfoot.pressor_values[3] + rfoot.pressor_values[4])) / rsum
#     # print lfoot.pressor_values[1], lfoot.pressor_values[2], lfoot.pressor_values[3], lfoot.pressor_values[4], rfoot.pressor_values[1], rfoot.pressor_values[2], rfoot.pressor_values[3], rfoot.pressor_values[4]
#     time.sleep(0.05)


# stop

# poppy.power_up()
# poppy.goto_position({  'l_ankle_x':np.degrees(t2 - np.pi / 2.0),
#                        'r_ankle_x':np.degrees(t1 - np.pi / 2.0),
#                        'l_hip_x': np.degrees(t4 - np.pi / 2.0) - 4,
#                        'r_hip_x': np.degrees(np.pi / 2.0 - t3) + 4,
#                        'abs_x': np.degrees( - t5 )
#                    },
#                         2.0,
#                     wait=False)

poppy.goto_position({  'l_ankle_x':np.degrees(t2 - np.pi / 2.0),
                       'r_ankle_x':np.degrees(t1 - np.pi / 2.0),
                       'l_hip_x': np.degrees(t4 - np.pi / 2.0) - 4,
                       'r_hip_x': np.degrees(np.pi / 2.0 - t3) + 4,
                       'abs_x': np.degrees( - t5 ) + 5
                   },
                        1.0,
                    wait=True)


# for i in range(50):
#     # print lfoot.pressor_values[1] + lfoot.pressor_values[2] + lfoot.pressor_values[3] + lfoot.pressor_values[4], rfoot.pressor_values[1] + rfoot.pressor_values[2] + rfoot.pressor_values[3] + rfoot.pressor_values[4]
#     # print lfoot.pressor_values[1], lfoot.pressor_values[2], lfoot.pressor_values[3], lfoot.pressor_values[4], rfoot.pressor_values[1], rfoot.pressor_values[2], rfoot.pressor_values[3], rfoot.pressor_values[4]
#     print (lfoot.pressor_values[1] + lfoot.pressor_values[2] + lfoot.pressor_values[3] + lfoot.pressor_values[4]) / lsum, (rfoot.pressor_values[1] + rfoot.pressor_values[2] + rfoot.pressor_values[3] + rfoot.pressor_values[4]) / rsum
#     time.sleep(0.05)




duration = 0.2
up = 0.03

mj1 = min_jerk.MJTraj(0, up, duration)
mj2 = min_jerk.MJTraj(up, 0.005, duration)
# mj2 = min_jerk.MJTraj(up, 0.0, 3.0)
mj3 = min_jerk.MJTraj(0.005, 0.0, 2.0)

poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "left", mj1), 'mjleftup')
poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "left", mj2), 'mjleftdown')
poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "left", mj3), 'mjleftdown_end')

# poppy.l_ankle_x.pid = (0.3, 0, 0)

# poppy.goto_position({'abs_x': np.degrees( - t5 ) + 5},
#                     0.1,
#                     wait = True)

# poppy.abs_x.pid = (10, 0, 0)
# poppy.abs_x.goal_position = np.degrees( - t5 ) + 5

poppy.mjleftup.start()
poppy.mjleftup.wait_to_stop()

# time.sleep(2.0)

poppy.mjleftdown.start()
poppy.mjleftdown.wait_to_stop()

poppy.mjleftdown_end.start()
poppy.mjleftdown_end.wait_to_stop()



# for i in range(100):
#     print poppy.abs_x.present_position
#     time.sleep(0.1)


# poppy.goto_position({'l_hip_y': np.degrees( - hip),
#                         'l_knee_y': np.degrees( - knee),
#                         'l_ankle_y': np.degrees(ankle)},
#                         duration,
#                         wait=True)


# poppy.goto_position({'l_hip_y': np.degrees(0),
#                         'l_knee_y': np.degrees(0),
#                         'l_ankle_y': np.degrees(0)},
#                         duration,
#                         wait=True)


# poppy.goto_position({'r_hip_y': np.degrees( - hip),
#                         'r_knee_y': np.degrees( - knee),
#                         'r_ankle_y': np.degrees(ankle)},
#                         duration,
#                         wait=True)


# poppy.goto_position({'r_hip_y': np.degrees(0),
#                         'r_knee_y': np.degrees(0),
#                         'r_ankle_y': np.degrees(0)},
#                         duration,
#                         wait=True)


time.sleep(3.0)
poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()
