#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_log.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: vendredi, juin 27 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################


import pypot.robot

# import poppytools.primitive.basic_dev as basic_dev
import basic_dev
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev
import poppytools.utils.min_jerk

import logprimitive

# from poppytools.configuration.config import poppy_config
from poppytools.configuration.config_1doffeet import poppy_config

import time

import random

import numpy as np

import math




poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()


poppy.attach_primitive(basic_dev.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()


poppy.power_up()


tracked = ('base','lshoe', 'rshoe', 'lhip', 'rhip', 'abs', 'head')

poppy.attach_primitive(logprimitive.OptiLog(poppy, tracked, '192.168.0.4', '8989', 'testoptilog.dat'), 'optilog')

poppy.attach_primitive(logprimitive.MotorLog(poppy, ['abs_y'], 'testmotorlog.dat'), 'motorlog')

poppy.attach_primitive(logprimitive.IMULog(poppy, 'testimulog.dat'), 'imulog')

poppy.attach_primitive(kinematics_dev.TempSecurity(poppy), 'secu')

poppy.optilog.start()
poppy.motorlog.start()
poppy.imulog.start()

poppy.secu.start()

time.sleep(5)
