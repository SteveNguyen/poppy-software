#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_log.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: vendredi, juin 27 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################


import pypot.robot

# import poppytools.primitive.basic_dev as basic_dev
import basic_dev
import poppytools.utils.kinematics as kinematics
# import poppytools.utils.kinematics_dev as kinematics_dev
# import poppytools.utils.min_jerk as min_jerk

import kinematics_dev
import min_jerk


import logprimitive

# from poppytools.configuration.config import poppy_config
from poppytools.configuration.config_1doffeet import poppy_config

import time

import random

import numpy as np

import math


import sys




poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()


poppy.attach_primitive(basic_dev.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()


poppy.power_up()



#Higher gains

poppy.r_ankle_y.pid = (50, 50, 0)
poppy.l_ankle_y.pid = (50, 50, 0)

poppy.r_hip_x.pid = (50, 50, 0)
poppy.l_hip_x.pid = (50, 50, 0)

poppy.r_hip_y.pid = (30, 30, 0)
poppy.l_hip_y.pid = (30, 30, 0)

poppy.abs_y.pid = (20, 20, 0)
poppy.abs_x.pid = (20, 20, 0)


# time.sleep(0.1)
# poppy.r_knee_y.pid = (50, 50, 0)
# poppy.l_knee_y.pid = (50, 50, 0)




# poppy.attach_primitive(kinematics_dev.FastPID(poppy, 50, {'l_ankle_y': 0.0, 'r_ankle_y': 0.0}, kp = 0.5), 'pidankle')
# poppy.pidankle.start()
# time.sleep(3)
# poppy.pidankle.stop()



tracked = ('base','lshoe', 'rshoe', 'lhip', 'rhip', 'abs', 'head')



motors = [m.name for m in poppy.motors]



motorfile = 'motorlog_' + sys.argv[1] + '.dat'
imufile = 'imulog_'+ sys.argv[1] + '.dat'
optifile = 'optilog_'+ sys.argv[1] + '.dat'

poppy.attach_primitive(logprimitive.OptiLog(poppy, tracked, '192.168.0.4', '8989', optifile), 'optilog')
poppy.attach_primitive(logprimitive.MotorLog(poppy, motors, motorfile), 'motorlog')

poppy.attach_primitive(logprimitive.IMULog(poppy, imufile), 'imulog')

poppy.attach_primitive(kinematics_dev.TempSecurity(poppy), 'secu')

poppy.optilog.start()
poppy.motorlog.start()
poppy.imulog.start()

poppy.secu.start()

# time.sleep(5)



#attach primitives to move the legs

up_duration = 0.15
down_duration = 0.15

# up_duration = 1.5
# down_duration = 1.5


up = 0.03
# up = 0.05

mj1 = min_jerk.MJTraj(0, up, up_duration)
mj2 = min_jerk.MJTraj(up, 0, down_duration)


# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "left", mj1), 'mjleftup')
# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "left", mj2), 'mjleftdown')

# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "right", mj1), 'mjrightup')
# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "right", mj2), 'mjrightdown')

offset =- 3.0

# offset = 0
# offset =poppy.l_ankle_y.present_position
# print 'OFFSET', offset

poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "left", mj1,offset), 'mjleftup')
poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "left", mj2,offset), 'mjleftdown')

poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "right", mj1,offset), 'mjrightup')
poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "right", mj2,offset), 'mjrightdown')


#just to be sure
# poppy.mjleftdown.start()
# poppy.mjrightdown.start()
# poppy.mjrightdown.wait_to_stop()
# poppy.mjleftdown.wait_to_stop()


time.sleep(1)

# poppy.attach_primitive(min_jerk.MoveGprojMJ(poppy,-0.0, 0.5), 'gprojmj')
# poppy.gprojmj.start()
# poppy.gprojmj.wait_to_stop()


# poppy.l_ankle_x.goal_position =- 10
# poppy.r_ankle_x.goal_position =- 10


print poppy.l_knee_y.pid
print poppy.l_knee_y.present_position
print poppy.l_knee_y.goal_position
print poppy.l_knee_y.present_temperature
print poppy.l_knee_y.present_speed
print poppy.l_knee_y.goal_speed
print poppy.l_knee_y.present_load
print poppy.l_knee_y.present_voltage
print poppy.l_knee_y.torque_limit





# poppy.l_ankle_x.compliant = True
# poppy.r_ankle_x.compliant = True





# stop

# time.sleep(2)

#lift the left foot
# poppy.l_ankle_y.compliant = True
poppy.l_ankle_y.pid = (0.5, 0, 0)
# time.sleep(0.05)
poppy.mjleftup.start()
# time.sleep(0.2)
# time.sleep(0.07)

poppy.mjleftup.wait_to_stop()
# time.sleep(0.0)
poppy.l_hip_x.goal_position=15
# time.sleep(0.05)
# poppy.l_ankle_x.compliant = True
# poppy.r_ankle_x.compliant = True

# poppy.l_ankle_x.goal_position =- 15


#land the left foot
poppy.mjleftdown.start()
poppy.mjleftdown.wait_to_stop()
# poppy.l_ankle_y.compliant = False
poppy.l_ankle_y.pid = (50, 50, 0)

# poppy.l_ankle_x.compliant = False
# poppy.r_ankle_x.compliant = False



#test
# #lift the right foot
# poppy.r_ankle_y.pid = (0.25, 0, 0)
# poppy.mjrightup.start()
# poppy.mjrightup.wait_to_stop()
# poppy.r_hip_x.goal_position=- 15

# #land the right foot
# poppy.mjrightdown.start()
# poppy.mjrightdown.wait_to_stop()
# poppy.r_ankle_y.pid = (50, 50, 0)



time.sleep(1.0)
# poppy.gprojmj.add(0, 1)
# poppy.gprojmj.start()
# poppy.gprojmj.wait_to_stop()


time.sleep(1.0)
poppy.attach_primitive(basic_dev.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()
