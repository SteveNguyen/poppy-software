#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'up.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: lundi, juin 30 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_log.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: vendredi, juin 27 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################


import pypot.robot

# import poppytools.primitive.basic_dev as basic_dev
import basic_dev
import poppytools.utils.kinematics as kinematics
# import poppytools.utils.kinematics_dev as kinematics_dev
# import poppytools.utils.min_jerk as min_jerk

import kinematics_dev
import min_jerk


import logprimitive

# from poppytools.configuration.config import poppy_config
from poppytools.configuration.config_1doffeet import poppy_config

import time

import random

import numpy as np

import math


import sys




poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()







time.sleep(1)

poppy.attach_primitive(basic_dev.StandPosition(poppy), 'stand_position')
# poppy.attach_primitive(basic_dev.SafeStandPosition(poppy), 'stand_position')

poppy.stand_position.start()
poppy.stand_position.wait_to_stop()



poppy.power_up()

# poppy.l_knee_y.compliant = True

# time.sleep(1)
# stop

#Higher gains

poppy.r_ankle_y.pid = (50, 50, 0)
poppy.l_ankle_y.pid = (50, 50, 0)

poppy.r_hip_x.pid = (50, 50, 0)
poppy.l_hip_x.pid = (50, 50, 0)

poppy.r_hip_y.pid = (30, 30, 0)
poppy.l_hip_y.pid = (30, 30, 0)

poppy.abs_y.pid = (20, 20, 0)
poppy.abs_x.pid = (20, 20, 0)


poppy.r_knee_y.pid = (50, 50, 0)
poppy.l_knee_y.pid = (50, 50, 0)


# poppy.l_ankle_x.goal_position = 0
# poppy.r_ankle_x.goal_position =0


# poppy.l_ankle_x.goal_position =- 10
# poppy.r_ankle_x.goal_position =- 10



# poppy.l_knee_y.compliant = True

time.sleep(20)
