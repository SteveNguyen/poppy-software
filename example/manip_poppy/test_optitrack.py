#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_optitrack.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: vendredi, juin 27 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import time
import datetime
import pypot.sensor.optibridge as bridge


tracked = ('base','lshoe', 'rshoe', 'lhip', 'rhip', 'abs', 'head')


c = bridge.OptiTrackClient('192.168.0.4', '8989',tracked )

print 'OK'
c.start()

print 'RUN'


# fd = open('track.dat', 'w+')

time.sleep(0.5)
s = '# time '
obj = c.tracked_objects

# print obj
# for k in obj.keys():
#     # print k
#     s += k
#     s += ' '
# s += '\n'
for k in tracked:
    if k in obj.keys():

        # print k
        s += k
        s += ' '
    else:
        print "ERROR, no", k

s += '\n'

print s
# fd.write(s)

while True:
    # print 'test', c.tracked_objects
    s = ''
    obj = c.tracked_objects

    # print obj
    # print obj['abs']

    for k in obj.iteritems():

        tt = k[1][3].timetuple()

        # print k[1][3].microsecond, tt, time.mktime(tt)

        t = k[1][3].microsecond + 1000000.0 * k[1][3].second + 60 * 1000000.0 * k[1][3].minute

        s += str(t) + ' '#time
        break

    for i in tracked:
        val = []
        if i not in obj.keys():
            val = [99999, 99999, 99999]
        else:
            val = obj[i][0]

        s += str(val[0]) + ' ' + str(val[1]) + ' ' +  str(val[2]) + ' '

    # for i in c.tracked_objects.iteritems():
    #     s += str(i[1][0][0]) + ' ' + str(i[1][0][1]) + ' ' + str(i[1][0][2]) +



        ' '
        # print i[1][3]
    s += '\n'
    print s
    # fd.write(s)
    #     print i
    # print obj
    # for k in obj.keys():
    #     print k

    # for k in obj.keys():
    #     print obj[k]


    time.sleep(0.01)
