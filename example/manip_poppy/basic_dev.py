import time
import itertools

import pypot.primitive


class InitRobot(pypot.primitive.Primitive):
    def setup(self):
        print("initialisation")

        self.robot.compliant = False

        self.robot.power_max()

        # Change PID of Dynamixel MX motors
        for m in filter(lambda m: hasattr(m, 'pid'), self.robot.motors):
            m.pid = (4, 2, 0)

        # Reduce max torque to keep motor temperature low
        for m in self.robot.motors:
            m.torque_limit = 70

        # for m in self.robot.torso:
        #     m.pid = (6, 2, 0)

        for m in self.robot.torso:
            m.pid = (4, 2, 0)

        time.sleep(0.5)


class StandPosition(InitRobot):

    def run(self):
        # Goto to position 0 on all motors
        # self.robot.goto_position(dict(zip((m.name for m in self.robot.motors),
        #                                     itertools.repeat(0))),
        #                                     2)

        # Specified some motor positions to keep the robot balanced
        self.robot.goto_position({'r_hip_z': -2,#was -2
                                'l_hip_z': 2,#was 2
                                'r_hip_x': -10,#was -2
                                'l_hip_x': 10,#was 2
                                  # 'l_ankle_x':- 7,
                                  # 'r_ankle_x':- 7,
                                'r_hip_y': 4,#was 4
                                'l_hip_y': 4,#was 4
                                'l_shoulder_x': 10,
                                'r_shoulder_x': -10,
                                'l_shoulder_y': 10,
                                'r_shoulder_y': 10,
                                'l_elbow_y': -20,
                                'r_elbow_y': -20,
                                'l_ankle_y':- 3.0, #was-0.5
                                'r_ankle_y':- 3.0,
                                'abs_y': 0,#was 0
                                'abs_x': 2,#was 0
                                'bust_x':0,#was 0
                                'head_y': 0,
                                'head_z':0},
                                3,
                                wait=True)



        # Restore the motor speed
        self.robot.power_max()

        # Reduce max torque to keep motor temperature low
        # for m in self.robot.motors:
        #     m.torque_limit = 70


        for m in self.robot.motors:
            # m.compliant = False
            m.torque_limit = 100

        time.sleep(0.1)

        # self.robot.l_hip_y.torque_limit = 20
        # self.robot.r_hip_y.torque_limit = 20



class SafeStandPosition(pypot.primitive.Primitive):
    def setup(self):
        print("Safe initialisation")


        init_pos = {'r_hip_z': -2,
                    'l_hip_z': 2,
                    'r_hip_x': -2,
                    'l_hip_x': 2,
                    'r_hip_y': 0,
                    'l_hip_y': 0,
                    'l_shoulder_x': 10,
                    'r_shoulder_x': -10,
                    'l_shoulder_y': 0,
                    'r_shoulder_y': 0,
                    'l_arm_z': 0,
                    'r_arm_z': 0,
                    'l_elbow_y': -20,
                    'r_elbow_y': -20,
                    'l_ankle_y': 1,
                    'r_ankle_y': 1,
                    # 'l_ankle_x': 0,
                    # 'r_ankle_x': 0,
                    'l_knee_y': 0,
                    'r_knee_y': 0,
                    'abs_y': 2,
                    'abs_x': 0,
                    'abs_z': 0,
                    'head_y': 0,
                    'head_z':0,
                    'bust_x': 0,
                    'bust_y': 0,
                    }


        # self.robot.compliant = False #useless?

        #check if we are far from the init pos

        for motorname, pos in init_pos.iteritems():
            m = getattr(self.robot, motorname)
            if(abs(m.present_position - pos) > 10):
                m.compliant = True
                print motorname, ' compliant'
            else:
                m.compliant = False

        # for m in self.robot.motors:
        #     m.compliant = True
        # self.robot.power_max()

        self.robot.l_shoulder_y.compliant = False
        self.robot.r_shoulder_y.compliant = False
        self.robot.l_hip_x.compliant = False
        self.robot.r_hip_x.compliant = False
        self.robot.l_hip_z.compliant = False
        self.robot.r_hip_z.compliant = False

        # Change PID of Dynamixel MX motors
        for m in filter(lambda m: hasattr(m, 'pid'), self.robot.motors):
            m.pid = (4, 2, 0)

        # Reduce max torque to keep motor temperature low
        for m in self.robot.motors:
            m.torque_limit = 70

        # for m in self.robot.torso:
        #     m.pid = (6, 2, 0)

        for m in self.robot.torso:
            m.pid = (4, 2, 0)

        time.sleep(0.5)


        self.robot.goto_position({'r_hip_z': 0,
                        'l_hip_z': 0,
                        'r_hip_x': 0,
                        'l_hip_x': 0,
                        'l_shoulder_y': 0,
                        'r_shoulder_y': 0},
                        3,
                                 wait=False)
        time.sleep(3)


    def run(self):


        # # Goto to position 0 on all motors
        # self.robot.goto_position(dict(zip((m.name for m in self.robot.motors),
        #                                     itertools.repeat(0))),
        #                                     3,wait=True)
        # self.robot.goto_position({'r_hip_z': -2,
        #                         'l_hip_z': 2,
        #                         'r_hip_x': -2,
        #                           'l_hip_x': 2,
        #                         'r_hip_y': 0,
        #                         'l_hip_y': 0,
        #                         'l_shoulder_x': 10,
        #                         'r_shoulder_x': -10,
        #                         'l_shoulder_y': 10,
        #                         'r_shoulder_y': 10,
        #                         'l_elbow_y': -20,
        #                         'r_elbow_y': -20,
        #                         'l_ankle_y': 1,
        #                         'r_ankle_y': 1,
        #                         'abs_y': 2,
        #                         'head_y': 0,
        #                         'head_z':0},
        #                         4,
        #                          wait=False)

        time.sleep(0.5)


        self.robot.compliant = False
        # self.robot.power_max()

        # Change PID of Dynamixel MX motors
        for m in filter(lambda m: hasattr(m, 'pid'), self.robot.motors):
            m.pid = (1, 0, 0)

        for m in self.robot.motors:
            m.compliant = False

        # Restore the motor speed
        self.robot.power_max()

        # Reduce max torque to keep motor temperature low
        for m in self.robot.motors:
            m.torque_limit = 70

        # self.robot.l_hip_y.torque_limit = 20
        # self.robot.r_hip_y.torque_limit = 20

        # self.robot.goto_position({'r_hip_z': -2,
        #                         'l_hip_z': 2,
        #                         'r_hip_x': -2,
        #                           'l_hip_x': 2,
        #                         'r_hip_y': 0,
        #                         'l_hip_y': 0,
        #                         'l_shoulder_x': 10,
        #                         'r_shoulder_x': -10,
        #                         'l_shoulder_y': 10,
        #                         'r_shoulder_y': 10,
        #                         'l_elbow_y': -20,
        #                         'r_elbow_y': -20,
        #                         'l_ankle_y': 1,
        #                         'r_ankle_y': 1,
        #                         'abs_y': 2,
        #                         'head_y': 0,
        #                         'head_z':0},
        #                         5,
        #                          wait=False)
        # time.sleep(4)

        for i in range(5):

            for m in filter(lambda m: hasattr(m, 'pid'), self.robot.motors):
                m.pid = (i + 1, 2, 0)


            self.robot.goto_position(dict(zip((m.name for m in self.robot.motors),
                                            itertools.repeat(0))),
                                     4,wait=False)

            self.robot.goto_position({'r_hip_z': -2,
                                    'l_hip_z': 2,
                                    'r_hip_x': -2,
                                      'l_hip_x': 2,
                                    'r_hip_y': 0,
                                    'l_hip_y': 0,
                                    'l_shoulder_x': 10,
                                    'r_shoulder_x': -10,
                                    'l_shoulder_y': 0,
                                    'r_shoulder_y': 0,
                                    'l_elbow_y': -20,
                                    'r_elbow_y': -20,
                                    'l_ankle_y': 1,
                                    'r_ankle_y': 1,
                                    'abs_y': 2,
                                    'head_y': 0,
                                    'head_z':0},
                                    3,
                                     wait=False)

            time.sleep(1)
        time.sleep(1)
        print 'Safe init done'

class SitPosition(pypot.primitive.Primitive):
    def run(self):
        self.robot.l_hip_y.goal_position = -35
        self.robot.r_hip_y.goal_position = -35
        self.robot.l_knee_y.goto_position(125, 2)
        self.robot.r_knee_y.goto_position(125, 2, wait=True)

        for m in self.robot.torso:
            m.goal_position = 0

        self.robot.abs_y.goal_position = 7

        motor_list = [self.robot.l_knee_y, self.robot.l_ankle_y, self.robot.r_knee_y, self.robot.r_ankle_y]

        for m in motor_list:
            m.compliant = True

        time.sleep(2)


class QuadPosition(pypot.primitive.Primitive):

    def run(self):


        # # Specified some motor positions to keep the robot balanced
        # self.robot.goto_position({'r_hip_z': -2,
        #                           'l_hip_z': 2,
        #                           'r_hip_y': -90,
        #                           'l_hip_y':- 90,
        #                           'r_hip_x': -2,
        #                           'l_hip_x': 2,
        #                           'r_knee_y': 118,
        #                           'l_knee_y': 118,
        #                           'l_ankle_y':- 50,
        #                           'r_ankle_y':- 50,
        #                           'l_ankle_x': 0,
        #                           'r_ankle_x': 0,
        #                           'l_shoulder_x': 10,
        #                           'r_shoulder_x': -10,
        #                           'l_shoulder_y':- 45,
        #                           'r_shoulder_y':- 45,
        #                           'l_elbow_y': -110,
        #                           'r_elbow_y': -110,
        #                           'l_arm_z': 0,
        #                           'r_arm_z': 0,
        #                           'abs_y': 2,
        #                           'abs_x': 0,
        #                           'abs_z': 0,
        #                           'bust_y': 0,
        #                           'bust_x': 0,
        #                           'head_y': 0,
        #                           'head_z':0
        #                       },
        #                          3,
        #                         wait=True)

        # Specified some motor positions to keep the robot balanced
        self.robot.goto_position({'r_hip_z': -20,
                                  'l_hip_z': 20,
                                  'r_hip_y': -70,
                                  'l_hip_y':- 70,
                                  'r_hip_x': -2,
                                  'l_hip_x': 2,
                                  'r_knee_y': 118,
                                  'l_knee_y': 118,
                                  'l_ankle_y':- 55,
                                  'r_ankle_y':- 55,
                                  'l_ankle_x': 0,
                                  'r_ankle_x': 0,
                                  'l_shoulder_x': 10,
                                  'r_shoulder_x': -10,
                                  'l_shoulder_y':- 40,
                                  'r_shoulder_y':- 40,
                                  'l_elbow_y': -50,
                                  'r_elbow_y': -50,
                                  'l_arm_z': 0,
                                  'r_arm_z': 0,
                                  'abs_y':- 4,
                                  'abs_x': 0,
                                  'abs_z': 0,
                                  'bust_y':- 20,
                                  'bust_x': 0,
                                  'head_y': 0,
                                  'head_z':0
                              },
                                 3,
                                 wait=True)



        # Restore the motor speed
        self.robot.power_max()

        # Reduce max torque to keep motor temperature low
        for m in self.robot.motors:
            m.torque_limit = 70

        # self.robot.l_hip_y.torque_limit = 20
        # self.robot.r_hip_y.torque_limit = 20
        self.robot.abs_z.compliant = True
