#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'logprimitive.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: vendredi, juin 27 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################


import pypot.primitive


import time
import datetime
import pypot.sensor.optibridge as bridge
import poppytools.sensor.inertial_unit as inertial




class OptiLog(pypot.primitive.LoopPrimitive):
    def __init__(self, robot, tracked, ip, port, filename, default=99999, refresh_freq = 100):
        pypot.primitive.LoopPrimitive.__init__(self, robot,refresh_freq)

        self.tracked = tracked
        self.opti_ip = ip
        self.opti_port = port
        self.opti = bridge.OptiTrackClient(self.opti_ip, self.opti_port,tracked )
        self.filename = filename
        self.fd = open(filename, 'w+')

        self.opti.start()
        self.default = default
        self.init()

    def init(self):

        time.sleep(0.5)

        s = '# time '
        obj = self.opti.tracked_objects

        for k in self.tracked:
            if k in obj.keys():

                # print k
                s += k
                s += ' '
            else:
                print "ERROR, no", k

        s += '\n'

        print s
        self.fd.write(s)



    def update(self):



        s = ''
        obj = self.opti.tracked_objects

        for k in obj.iteritems():

            tt = k[1][3].timetuple()

            # print k[1][3].microsecond, tt, time.mktime(tt)

            t = k[1][3].microsecond + 1000000.0 * k[1][3].second + 60 * 1000000.0 * k[1][3].minute

            s += str(t) + ' '#time
            break


        for i in self.tracked:
            val = []
            if i not in obj.keys():
                val = [self.default, self.default, self.default ]
            else:
                val = obj[i][0]

            s += str(val[0]) + ' ' + str(val[1]) + ' ' +  str(val[2]) + ' '


        s += '\n'
        self.fd.write(s)

    def stop(self):
        self.fd.close()


class MotorLog(pypot.primitive.LoopPrimitive):
    def __init__(self, robot, motorlist, filename, refresh_freq = 50):

        pypot.primitive.LoopPrimitive.__init__(self, robot,refresh_freq)

        self.motorlist = motorlist
        self.filename = filename
        self.fd = open(filename, 'w+')

        self.init()

    def init(self):

        # time.sleep(0.5)

        s = '# time '


        for k in self.motorlist:


            # print k
            s += k
            s += ' '


        s += '\n'

        print s
        self.fd.write(s)



    def update(self):

        s = str(time.time()) + ' '

        for motor in self.motorlist:
            m = getattr(self.robot, motor)

            s += str(m.present_position) + ' ' + str(m.goal_position) + ' '

        s += '\n'
        self.fd.write(s)

    def stop(self):
        self.fd.close()


class IMULog(pypot.primitive.LoopPrimitive):
    def __init__(self, robot, filename, port='/dev/poppy_imu', refresh_freq = 100):

        pypot.primitive.LoopPrimitive.__init__(self, robot,refresh_freq)

        self.port = port
        self.filename = filename
        self.fd = open(filename, 'w+')
        self.imu = inertial.Imu('/dev/poppy_imu')


        self.init()

    def init(self):
        self.imu.start()
        # time.sleep(0.5)

        s = '# time acc gyro tilt\n'
        self.fd.write(s)



    def update(self):

        s = str(time.time()) + ' '


        s += str(self.imu.acc.x) + ' ' + str(self.imu.acc.y) + ' ' + str(self.imu.acc.z) + ' '
        s += str(self.imu.gyro.x) + ' ' + str(self.imu.gyro.y) + ' ' + str(self.imu.gyro.z) + ' '
        s += str(self.imu.tilt.x) + ' ' + str(self.imu.tilt.y) + ' ' + str(self.imu.tilt.z)

        s += '\n'
        self.fd.write(s)

    def stop(self):
        self.fd.close()
