#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'remote_test.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@inria.fr
#  Created	: Tuesday, September 23 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot
from pypot.robot.remote import from_remote
from poppytools.configuration.config import poppy_config_1doffeet as poppy_config

from poppytools.primitive.interaction import SmartCompliance
import numpy as np
import time

poppy = pypot.robot.from_config(poppy_config)
poppy_remote = from_remote('macpro.local', 1234)

poppy.start_sync()


motor_list = []
for m in poppy.motors:
    motor_list.append(m)

poppy.attach_primitive(SmartCompliance(poppy, poppy.arms), 'smart_compliance')
poppy.smart_compliance.start()



for m in poppy_remote.motors:
    m.compliant = False


pos = {}
while True:
    # poppy_remote.r_elbow_y.goal_position = poppy.r_elbow_y.present_position
    for m in poppy.motors:

        mr = getattr(poppy_remote, m.name)
        ml = getattr(poppy, m.name)
        # print mr, ml
        if not m.name in pos:
            pos[m.name] = ml.present_position
            mr.goal_position = ml.present_position

        else:
            if abs(ml.present_position - pos[m.name]) > 0.5:
                pos[m.name] = ml.present_position
                mr.goal_position = ml.present_position



    time.sleep(0.02)
