#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config
import poppytools.utils.min_jerk as min_jerk

import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config
import math

# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()


lfoot = pressure.FootPressure('/dev/poppy_lfoot')
rfoot = pressure.FootPressure('/dev/poppy_rfoot')

lfoot.start()
rfoot.start()




poppy.power_up()




poppy.l_ankle_x.pid = (0.3, 0, 0)
poppy.r_ankle_x.pid = (0.3, 0, 0)


# c, t1, t2, t3, t4, t5 = kinematics_dev.get_para( 0.025 )#+ 0.075)

# print c, np.degrees(t1), np.degrees(t2), np.degrees(t3 - np.pi / 2.0), np.degrees(t4 - np.pi / 2.0), np.degrees(t5)

# lsum = []
# rsum = []

# print "init"
# for i in range(50):

#     lsum.append(lfoot.pressor_values[1] + lfoot.pressor_values[2] + lfoot.pressor_values[3] + lfoot.pressor_values[4])
#     rsum.append(rfoot.pressor_values[1] + rfoot.pressor_values[2] + rfoot.pressor_values[3] + rfoot.pressor_values[4])
#     time.sleep(0.05)


# lsum = np.nansum(lsum) / (np.count_nonzero(~np.isnan(lsum)))
# rsum = np.nansum(rsum) / (np.count_nonzero(~np.isnan(rsum)))



# print "init done ", lsum, rsum

# for i in range(500):
#     print (lfoot.pressor_values[1] + lfoot.pressor_values[2] + lfoot.pressor_values[3] + lfoot.pressor_values[4]) / lsum, (rfoot.pressor_values[1] + rfoot.pressor_values[2] + rfoot.pressor_values[3] + rfoot.pressor_values[4]) / rsum, ((lfoot.pressor_values[1] + lfoot.pressor_values[2]) - (lfoot.pressor_values[3] + lfoot.pressor_values[4])) / lsum, ((rfoot.pressor_values[1] + rfoot.pressor_values[2]) - (rfoot.pressor_values[3] + rfoot.pressor_values[4])) / rsum, ((lfoot.pressor_values[1] + lfoot.pressor_values[2]) - (lfoot.pressor_values[3] + lfoot.pressor_values[4])) / lsum + ((rfoot.pressor_values[1] + rfoot.pressor_values[2]) - (rfoot.pressor_values[3] + rfoot.pressor_values[4])) / rsum
#     # print lfoot.pressor_values[1], lfoot.pressor_values[2], lfoot.pressor_values[3], lfoot.pressor_values[4], rfoot.pressor_values[1], rfoot.pressor_values[2], rfoot.pressor_values[3], rfoot.pressor_values[4]
#     time.sleep(0.05)


# stop

# # poppy.power_up()
# poppy.goto_position({  'l_ankle_x':np.degrees(t2 - np.pi / 2.0),
#                        'r_ankle_x':np.degrees(t1 - np.pi / 2.0),
#                        'l_hip_x': np.degrees(t4 - np.pi / 2.0) - 4,
#                        'r_hip_x': np.degrees(np.pi / 2.0 - t3) + 4,
#                        'abs_x': np.degrees( - t5)
#                    },
#                         2.0,
#                     wait=False)

# for i in range(200):
#     # print lfoot.pressor_values[1] + lfoot.pressor_values[2] + lfoot.pressor_values[3] + lfoot.pressor_values[4], rfoot.pressor_values[1] + rfoot.pressor_values[2] + rfoot.pressor_values[3] + rfoot.pressor_values[4]
#     print lfoot.pressor_values[1], lfoot.pressor_values[2], lfoot.pressor_values[3], lfoot.pressor_values[4], rfoot.pressor_values[1], rfoot.pressor_values[2], rfoot.pressor_values[3], rfoot.pressor_values[4]
#     time.sleep(0.05)





# poppy.attach_primitive(min_jerk.goto_mj(poppy, {'r_shoulder_y':- 45}, 1.0), 'mj')
# poppy.mj.start()
# poppy.mj.wait_to_stop()

# poppy.mj.add({'r_shoulder_y':0}, 1.0)
# poppy.mj.start()
# poppy.mj.wait_to_stop()


# poppy.l_hip_x.pid = (0.0, 0, 0)
# poppy.r_hip_x.pid = (0.0, 0, 0)
# poppy.abs_y.pid = (0.0, 0, 0)

# for i in range(100):
#     db = kinematics_dev.get_db(poppy)
#     time.sleep(0.5)

# g = poppy.r_shoulder_y.present_position
# for i in range(1000):
#     g = poppy.r_shoulder_y.present_position + (poppy.r_shoulder_y.present_position - g)
#     print (poppy.r_shoulder_y.present_position - g)
#     poppy.r_shoulder_y.goal_position = g
#     time.sleep(0.02)





# poppy.attach_primitive(kinematics_dev.MoveGprojMJ(poppy,0.0, 2.0), 'gprojmj')
# poppy.gprojmj.start()
# poppy.gprojmj.wait_to_stop()

# poppy.gprojmj.add(0, 2.0)
# poppy.gprojmj.start()
# poppy.gprojmj.wait_to_stop()


# poppy.attach_primitive(min_jerk.goto_mj(poppy, kinematics_dev.get_inv_gproj_angles(poppy, 0.03), 2.0), 'mj')
# poppy.mj.start()
# poppy.mj.wait_to_stop()

# poppy.mj.add(kinematics_dev.get_inv_gproj_angles(poppy, 0.0), 2.0)
# poppy.mj.start()
# poppy.mj.wait_to_stop()


min_jerk.yes_no(poppy, 'y', wait = True)
time.sleep(1.0)
min_jerk.yes_no(poppy, 'n', wait = True)




# time.sleep(3.0)
# poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()
