#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure

import poppytools.utils.min_jerk as min_jerk

from poppytools.primitive.interaction import ArmsCompliant, ArmsCopyMotion, SmartCompliance,  ArmsTurnCompliant

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config

import robot_gui

import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config
import math
import wx

# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



poppy.attach_primitive(basic.SafeStandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()



lfoot = pressure.FootPressure('/dev/poppy_lfoot')
rfoot = pressure.FootPressure('/dev/poppy_rfoot')

lfoot.start()
rfoot.start()




poppy.power_up()






# poppy.compliant = True

# for i in range(10000):
#     print kinematics_dev.get_db(poppy)
#     time.sleep(0.02)
# stop



#Slow PID on the abs to keep it straight
poppy.attach_primitive(kinematics_dev.SlowPID(poppy, 50, {'abs_y': 0.0}), 'spid')
poppy.spid.start()



#allow for a compliance of the ankle x
poppy.l_ankle_x.pid = (0.25, 0, 0)
poppy.r_ankle_x.pid = (0.25, 0, 0)

#just to be sure we are in a straight position

poppy.attach_primitive(min_jerk.MoveGprojMJ(poppy,0.0, 2.0), 'gprojmj')
poppy.gprojmj.start()
poppy.gprojmj.wait_to_stop()


# #higher gain for ankle y
# poppy.l_ankle_y.pid = (10, 2, 0)
# poppy.r_ankle_y.pid = (10, 2, 0)

# # poppy.l_hip_x.pid = (20, 0, 0)
# # poppy.r_hip_x.pid = (20, 0, 0)

# poppy.abs_x.pid = (10, 0, 0)
# poppy.abs_y.pid = (10, 0, 0)



#find the equilibrium point
# poppy.attach_primitive(kinematics_dev.CompensateFoot(poppy, 50, 3.0, continuous = False), 'Compensate')
# poppy.Compensate.start()
# poppy.Compensate.wait_to_stop()

#attach primitives to move the legs


up_duration = 0.1
down_duration = 0.15

up = 0.0025
# up = 0.00


# duration = 2.0
# up = 0.05


mj1 = min_jerk.MJTraj(0, up, up_duration)
mj2 = min_jerk.MJTraj(up, 0, down_duration)


# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "left", mj1), 'mjleftup')
# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "left", mj2), 'mjleftdown')

# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "right", mj1), 'mjrightup')
# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "right", mj2), 'mjrightdown')


# poppy.mjleftup.start()
# poppy.mjleftup.wait_to_stop()

# poppy.mjleftdown.start()
# poppy.mjleftdown.wait_to_stop()



#ok we are ready
min_jerk.yes_no(poppy, 'y', wait = True)
time.sleep(1.0)


# X_COM = 0.015
X_COM = 0.025 #pas mal sans footreact
# X_COM = 0.022

COM_DURATION = 1

STATE = 'LEFT'


RATIO = 1

poppy.abs_y.goal_position = 1.0
poppy.l_ankle_y.goal_position = 1.0
poppy.r_ankle_y.goal_position = 1.0



poppy.r_ankle_y.pid = (60, 30, 0)
poppy.l_ankle_y.pid = (20, 20, 0)

poppy.r_hip_x.pid = (60, 60, 0)
poppy.l_hip_x.pid = (60, 60, 0)

poppy.r_hip_y.pid = (60, 60, 0)
poppy.l_hip_y.pid = (60, 60, 0)

poppy.abs_y.pid = (60, 60, 0)
poppy.abs_x.pid = (60, 60, 0)



poppy.attach_primitive(SmartCompliance(poppy, poppy.arms, 50), 'arms_compliant')
# poppy.arms_compliant.start()



# poppy.attach_primitive(kinematics_dev.ControlFeetDist(poppy, strict=True),  'feetdist')
# poppy.feetdist.start()



poppy.attach_primitive(kinematics_dev.FootReact(poppy, lfoot, rfoot), 'footreact')
poppy.footreact.start()

# poppy.attach_primitive(kinematics_dev.MoveGprojSin(poppy, X_COM, 1.2), 'gsin') #pas mal sans footreact
# poppy.attach_primitive(kinematics_dev.MoveGprojSin(poppy, X_COM, 1.05), 'gsin')
poppy.attach_primitive(kinematics_dev.MoveGprojSin(poppy, 0, 1.0), 'gsin')

poppy.gsin.start()


app = wx.PySimpleApp()
frame = robot_gui.SinGUI(poppy.gsin, None, True)
app.MainLoop()



# time.sleep(10)

# poppy.gsin.stop()


# time.sleep(3.0)
poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()
