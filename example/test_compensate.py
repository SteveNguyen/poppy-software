#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config


import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config


# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()


# print poppy.l_ankle_y.orientation
# stop

poppy.l_ankle_y.pid = (7, 0, 0)
poppy.r_ankle_y.pid = (7, 0, 0)

# for i in range(100):
#     print poppy.l_ankle_y.present_position, poppy.r_ankle_y.present_position
#     time.sleep(0.2)

# stop



# poppy.attach_primitive(kinematics_dev.CompensateFoot(poppy, 50, {'l_ankle_y': 3, 'r_ankle_y': 3}), 'Compensate')
poppy.attach_primitive(kinematics_dev.CompensateFoot(poppy, 50, 3.0, continuous = True), 'Compensate')

poppy.Compensate.start()
# poppy.Compensate.wait_to_stop()



poppy.abs_y.pid = (7, 0, 0)
poppy.abs_x.pid = (7, 0, 0)

poppy.attach_primitive(kinematics_dev.Compensate(poppy, 50, {'abs_y': 0.0}, continuous = True), 'Compensate_abs')

poppy.Compensate_abs.start()
poppy.Compensate_abs.wait_to_stop()

poppy.Compensate.wait_to_stop()



# time.sleep(3.0)
# poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()
