import pypot.robot

import poppytools.primitive.basic as basic
from poppytools.configuration.config import poppy_config
import time

import poppytools.sensor.inertial_unit as inertial

import pickle

from pypot.primitive.utils import Sinus

from explauto.utils.config import make_configuration
from explauto.sensorimotor_model.imle import ImleModel

from numpy import array
import random

U_MIN =- 10.0
U_MAX = 10.0

POS_MIN =- 20.0
POS_MAX = 20.0

SPEED_MIN =- 100.0
SPEED_MAX = 100.0

LOAD_MIN =- 100.0
LOAD_MAX = 100.0


# conf = make_configuration(m_mins = array([U_MIN]), m_maxs = array([U_MAX]), s_mins = array([POS_MIN,POS_MIN, LOAD_MIN]), s_maxs = array([ POS_MAX, POS_MAX, LOAD_MAX]))
# conf = make_configuration(m_mins = array([U_MIN]), m_maxs = array([U_MAX]), s_mins = array([POS_MIN,POS_MIN]), s_maxs = array([ POS_MAX, POS_MAX]))
conf = make_configuration(m_mins = array([U_MIN]), m_maxs = array([U_MAX]), s_mins = array([POS_MIN,POS_MIN, SPEED_MIN]), s_maxs = array([ POS_MAX, POS_MAX, SPEED_MAX]))
# conf = make_configuration(m_mins = array([POS_MIN]), m_maxs = array([POS_MAX]), s_mins = array([U_MIN, LOAD_MIN]), s_maxs = array([ U_MAX, LOAD_MAX]))
from explauto.sensorimotor_model.nearest_neighbor import NearestNeighbor
# model = NearestNeighbor(conf, sigma_ratio = 0.)
model = ImleModel(conf)

err = []
th_err = []

f = open('data.dat', 'r')
for l in f:
    l = l.replace('\n', '').split(' ')
    goal = float(l[0])
    present = float(l[1])
    past = float(l[2])
    # print goal, present, past
    model.update([goal], [present, past, present - past])

f.close()
f = open('data.dat', 'r')


for l in f:
    l = l.replace('\n', '').split(' ')

    goal = float(l[0])
    present = float(l[1])
    past = float(l[2])
    # model.update([goal], [present, past])
    # print goal, present, past
    ginf = model.infer(conf.s_dims, conf.m_dims, [present,past, present - past])

    err.append((goal - ginf) )
    th_err.append((goal - present) )

    print goal, ginf, present, past, (goal - ginf)[0] , (goal - present)


fm = open('learnmodel.dat', 'w')
pickle.dump(model, fm)
fm.close()


err = array(err)
th_err = array(th_err)

print err.mean(), err.std(), th_err.mean(), th_err.std()



f.close()
