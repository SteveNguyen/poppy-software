#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot



import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure
import poppytools.sensor.inertial_unit as imu


from poppytools.configuration.config import poppy_config


import time
import random

import numpy as np

import math

import os

from pylab import *

#learnt parameters

#pitch
C1_X = -780.910402454
C2_X = 99.666667186
C3_X = 11.5803576209

RX_MEAN = -4.33383699298e-13
RX_STD = 45.742217462

#roll

C1_Y = 781.683792482
C2_Y = 57.8525050175
C3_Y = 15.0891417546

RY_MEAN = 5.96175355996e-13
RY_STD = 60.8766566527




poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()


poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()


lfoot = pressure.FootPressure('/dev/poppy_lfoot')
rfoot = pressure.FootPressure('/dev/poppy_rfoot')

imu = imu.Imu('/dev/poppy_imu')
imu.start()

lfoot.start()
rfoot.start()

time.sleep(3)


poppy.power_up()


def get_torques(ldata, rdata):

    lf = ldata[1] + ldata[2] + ldata[3] + ldata[4]
    rf = rdata[1] + rdata[2] + rdata[3] + rdata[4]

    xtorque = (ldata[1] + ldata[2] + rdata[1] + rdata[2]) - (ldata[3] + ldata[4] + rdata[3] + rdata[4])
    ytorque = lf - rf

    return xtorque, ytorque



poppy.l_ankle_x.pid = (0.3, 0, 0)
poppy.r_ankle_x.pid = (0.3, 0, 0)

poppy.abs_x.pid = (10, 0, 0)
poppy.abs_y.pid = (10, 0, 0)

poppy.l_ankle_y.pid = (10, 0, 0)
poppy.r_ankle_y.pid = (10, 0, 0)






poppy.attach_primitive(kinematics_dev.SlowPID(poppy, 50, {'l_ankle_y': 1.0, 'r_ankle_y': 1.0, 'abs_y': 5.0}), 'spid')
poppy.spid.start()



X_DC = C1_X
Y_DC = C1_Y

# ion()
# fig = figure()
# ax = fig.add_subplot(111)

rxplot = []
ryplot = []


# linex,= plot(rxplot)
# liney,= plot(ryplot)

XTIMER = 5

for i in range(1000):

    tx, ty = get_torques(lfoot.pressor_values, rfoot.pressor_values)
    pitch = (poppy.l_ankle_y.present_position + poppy.r_ankle_y.present_position) / 2.0
    roll = (poppy.l_ankle_x.present_position - poppy.r_ankle_x.present_position) / 2.0

    rx = tx - X_DC - C2_X * pitch - C3_X * imu.acc.y
    ry = ty - Y_DC - C2_Y * roll - C3_Y * imu.acc.x

    time.sleep(0.05)
    # print rx, ry

    #adhoc arm reaction
    if abs(rx - RX_MEAN) > 2.0 * RX_STD and XTIMER <= 0:
        print 'PITCH ', rx

        if abs(rx / 25.0) < 45.0:
            poppy.r_shoulder_y.goal_position = rx / 25.0 + 10
            poppy.l_shoulder_y.goal_position = rx / 25.0 + 10

        else:
            poppy.r_shoulder_y.goal_position = sign( rx) * 45.0
            poppy.l_shoulder_y.goal_position = sign( rx) * 45.0


        XTIMER = 5

    else:
        poppy.goto_position({  'r_shoulder_y':10,
                               'l_shoulder_y':10,
                   },
                        1.0,
                    wait=False)



    XTIMER -= 1

    if abs(ry - RY_MEAN) > 2.0 * RY_STD:
        print 'ROLL ', ry

    X_DC += rx
    Y_DC += ry

    # print tx

    # rxplot.append(rx)
    # if len(rxplot) > 50:
    #     rxplot.pop(0)

    # ryplot.append(ry)
    # if len(ryplot) > 50:
    #     ryplot.pop(0)


    # linex.set_xdata(range(len(rxplot)))
    # linex.set_ydata(rxplot)

    # liney.set_xdata(range(len(ryplot)))
    # liney.set_ydata(ryplot)


    # ax.relim()
    # ax.autoscale_view()
    # draw()

time.sleep(3.0)
poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()
