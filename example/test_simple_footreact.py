#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure
import poppytools.sensor.inertial_unit as inertial

import poppytools.utils.min_jerk as min_jerk

from poppytools.primitive.interaction import ArmsCompliant, ArmsCopyMotion, SmartCompliance,  ArmsTurnCompliant

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config


import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config
import math

# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



poppy.attach_primitive(basic.SafeStandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()



# lfoot = pressure.FootPressure('/dev/poppy_lfoot')
# rfoot = pressure.FootPressure('/dev/poppy_rfoot')

# lfoot.start()
# rfoot.start()



imu = inertial.Imu('/dev/poppy_imu')
imu.start()

poppy.power_up()


poppy.r_ankle_y.pid = (20, 20, 0)
poppy.l_ankle_y.pid = (20, 20, 0)

poppy.r_hip_x.pid = (60, 60, 0)
poppy.l_hip_x.pid = (60, 60, 0)

poppy.r_hip_y.pid = (60, 60, 0)
poppy.l_hip_y.pid = (60, 60, 0)

poppy.abs_y.pid = (60, 60, 0)
poppy.abs_x.pid = (60, 60, 0)


# poppy.abs_x.pid = (10, 2, 0)
# poppy.abs_y.pid = (6, 2, 0)

poppy.attach_primitive(kinematics_dev.StabilizeTrunkYHip(poppy, imu, 0), 'staby')
# poppy.staby.state = 'left'
poppy.staby.start()

poppy.attach_primitive(kinematics_dev.StabilizeTrunkXHip(poppy, imu, 0), 'stabx')
# poppy.stabx.state = "right"
poppy.stabx.start()

poppy.attach_primitive(kinematics_dev.SymmetricLegs(poppy), 'symlegs')
# poppy.symlegs.state = 'left'
poppy.symlegs.start()

# while True:

#     time.sleep(0.1)
# stop




#allow for a compliance of the ankle x
poppy.l_ankle_x.pid = (0.25, 0, 0)
poppy.r_ankle_x.pid = (0.25, 0, 0)


poppy.l_ankle_y.pid = (0.35, 0, 0)
poppy.r_ankle_y.pid = (0.35, 0, 0)


#attach primitives to move the legs


up_duration = 0.1
down_duration = 0.1
up = 0.05

# up = 0.00


# duration = 2.0
# up = 0.05


mj1 = min_jerk.MJTraj(0, up, up_duration)
mj2 = min_jerk.MJTraj(up, 0, down_duration)


poppy.attach_primitive(min_jerk.MJLegs1DSym(poppy, "left", mj1), 'mjleftup')
poppy.attach_primitive(min_jerk.MJLegs1DSym(poppy, "left", mj2), 'mjleftdown')

poppy.attach_primitive(min_jerk.MJLegs1DSym(poppy, "right", mj1), 'mjrightup')
poppy.attach_primitive(min_jerk.MJLegs1DSym(poppy, "right", mj2), 'mjrightdown')


# poppy.mjleftup.start()
# poppy.mjleftup.wait_to_stop()

# poppy.mjleftdown.start()
# poppy.mjleftdown.wait_to_stop()



#ok we are ready
min_jerk.yes_no(poppy, 'y', wait = True)
time.sleep(2.0)


poppy.attach_primitive(SmartCompliance(poppy, poppy.arms, 50), 'arms_compliant')
# poppy.arms_compliant.start()


# poppy.attach_primitive(kinematics_dev.FootReact(poppy, lfoot, rfoot), 'footreact')
# poppy.footreact.start()

STATE = 'DS'
PREV_STATE = 'NONE'


while True:
    # print poppy.footreact.losc, poppy.footreact.rosc

    print STATE
    if STATE == 'DS':
        # poppy.staby.state = 'both'
        # poppy.stabx.state = 'none'
        if PREV_STATE == 'LEFT' or PREV_STATE == 'NONE':

            STATE = 'RIGHT'
            PREV_STATE = 'DS'
            poppy.staby.state = 'right'
            poppy.stabx.state = 'right'
            poppy.symlegs.state = 'right'
            poppy.mjrightup.start()
            poppy.mjrightup.wait_to_stop()

            time.sleep(0.1)
            poppy.mjrightdown.start()
            poppy.mjrightdown.wait_to_stop()

        elif PREV_STATE == 'RIGHT':

            STATE = 'LEFT'
            PREV_STATE = 'DS'
            poppy.staby.state = 'left'
            poppy.stabx.state = 'left'
            poppy.symlegs.state = 'left'
            poppy.mjleftup.start()
            poppy.mjleftup.wait_to_stop()
            time.sleep(0.1)
            poppy.mjleftdown.start()
            poppy.mjleftdown.wait_to_stop()

    elif STATE == 'LEFT' or STATE == 'RIGHT':
        PREV_STATE = STATE
        STATE = 'DS'
        poppy.staby.state = 'both'
        poppy.stabx.state = 'none'
        poppy.symlegs.state = 'none'


    time.sleep(0.02)





# time.sleep(3.0)
poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()
