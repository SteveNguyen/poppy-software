$(document).ready(function() {

    var totalPoints = 100;
    var NB = 25;

    var roboturl="http://localhost:8080"
    var motorlisturl=roboturl+"/motor/list.json"

    var updateInterval=1000

    var motorlist=[]
    var motorpos={}
    var motorposbuffer={}

    var motorposprint=[]
    var plots={}

    var testdata=[[1,0],[2,1],[3,4],[4,10],[5,8],[6,3]]


    // var testplot= $.plot("#testplot",testdata , {
    // // var testplot= $.plot("#testplot", [1,2,3,5,9,7,5], {
    // 	label:"test",
    // 	series: {
    // 	    lines: { show: true },
    //         points: { show: true },
    //         shadowSize: 0   // Drawing is faster without shadows
    // 	},

    // 	yaxis: {
    // 	    min: -180,
    // 	    max: 180
    // 	}



    // });



    // var ni = document.getElementById('motors');
    // var innerDiv = document.createElement('div');
    // innerDiv.className = 'demo-placeholder';
    // innerDiv.id='abs_y';
    // ni.appendChild(innerDiv);



    function getMotorList()
    {

        $.getJSON(motorlisturl,
		  function(data,status,xhr) {
		      var key='motors'
		      console.log(data[key]);
		      motorlist=data[key];
		      for(k in data[key])
		      {
			  // console.log(data[key][k]);
			  motorposbuffer[data[key][k]]=[0.0];



			  // console.log(data[key][k]);

			  // plots[data[key][k]]= $.plot("#motor_position",motorposbuffer[data[key][k]] ,
			  // 			      {
			  // 				  label:data[key][k],
			  // 				  series: {
			  // 				      shadowSize: 0   // Drawing is faster without shadows
			  // 				  },
			  // 				  yaxis: {
			  // 				      min: -180,
			  // 				      max: 180
			  // 				  },
			  // 				  xaxis: {
			  // 				      show: false
			  // 				  }
			  // 			      });


			  // var div="<div id=\" "+key+"class=\"demo-placeholder\"></div>"
			  // var div="<div id= \"abs_y\" class=\"demo-placeholder\"></div>"
			  // $('#motors').html(div);

			  // var ni = document.getElementById('motors');
			  // var innerDiv = document.createElement('div');
			  // innerDiv.className = 'demo-placeholder';
			  // innerDiv.id='abs_y';
			  // ni.appendChild(innerDiv);

		      }
		  }
		 )

    }

    function update() {
        getPos();
        // getTemp();
        // getSens();

	// create_plots();

	printPos();
	// var d=motorposbuffer['l_shoulder_y'];
	// console.log(d);
	// testplot.setData(formatData(motorposbuffer['l_shoulder_y']));
	// testplot.setData(formatData([5,6,4,8,6,1,4,6,361,654,24],1));
	// testplot.draw( [[1,0],[2,1],[3,4],[4,10],[5,8],[6,3]]);
	testplot.draw();
	// for(k in motorposbuffer)
	// {
	//     plots[k].setData(motorposbuffer[k]);
	//     plots[k].draw();

	// }

	// plot_position.setData(formatData(position_data, NB));
        // plot_temperature.setData(formatData(temperature_data, NB));
        // plot_sensor.setData(formatData(sensor_data, nb_sensor));

        // plot_position.draw();
        // plot_temperature.draw();
        // plot_sensor.draw();

	// console.log(motorpos);
        setTimeout(update, updateInterval);
    }



    getMotorList();
    var testplot= $.plot("#abs_y",[{label:"test",data:[[1,0],[2,1],[3,4],[4,10],[5,8],[6,3]]}]);
    // create_plots();


    update();


    function create_plots()
    {


	// motorlist.forEach(
	//     function(entry)
	//     {

	// 	// console.log('plots '+entry);
	// 	// console.log(motorposbuffer[entry]);
	// 	plots[entry]= $.plot("#motor_position",motorposbuffer[entry] ,
	// 		     {
	// 		  	 label:entry,
	// 		  	 series: {
	// 		  	     shadowSize: 0   // Drawing is faster without shadows
	// 		  	 },
	// 		  	 yaxis: {
	// 		  	     min: -180,
	// 		  	     max: 180
	// 		  	 },
	// 		  	 xaxis: {
	// 		  	     show: false
	// 		  	 }
	// 		     });
	// 	console.log(plots[entry]);
	//     }
	// );
    }


    function getPos()
    {

	motorlist.forEach(
	    function(entry)
	    {
		$.getJSON(roboturl+'/motor/'+entry+'/present_position',
			  function(data,status,xhr) {
			      motorpos[entry]=data['present_position'];


			      motorposbuffer[entry].push(data['present_position'])
			      if(motorposbuffer[entry].length>totalPoints)
			      {
			      	  motorposbuffer[entry].shift();
			      }

			  }


			 );

	    }

	);

    }

    function printPos()
    {
	motorposprint=[]
	for(var key in motorpos)
	{
	    // console.log(key+': '+motorpos[key]);

	    motorposprint.push('<p>' + key + ': '+motorpos[key] + '</p>');
	    // motorposprint.push('<p>' + key + ': '+motorposbuffer[key] + '</p>');
	    // console.log(motorposbuffer);


	}

    	$('#motors').html(motorposprint);



    }


    function formatData(data) {



        var res = [];

            // for (var i= 0; i < data.length; i++) {
        //     res.push([i, data[i][nb]]);
        // }
	for (var i= 0; i < 100; i++) {
            res.push([i, 42]);
        }

	console.log(res);

        return res;
    }

    // function formatData(data, nb_max) {
    //     var mega_res = [];

    //     for (var nb = 0; nb < nb_max; nb ++) {
    //         var res = [];

    //         for (var i= 0; i < data.length; i++) {
    //             res.push([i, data[i][nb]]);
    //         }

    //         mega_res.push(res);
    //     }

    //     return mega_res;
    // }


    // $("#driver").click(function(event){



    //     $.getJSON(motorlisturl,
    // 		  function(data,status,xhr) {
    // 		      console.log(data);
    // 		      console.log(status);
    // 		      console.log(xhr);
    // 		      var items = [];
    // 		      var key='motors'
    // 		      // $.each( data, function( key, val ) {
    // 		      // 	  items.push( '<p>'+ val +'</p>'  );
    // 		      // 	  // $('#motors').html(item);
    // 		      // });

    // 		      // console.log(data[key]);

    // 		      console.log(data[key]);
    // 		      (data[key]).forEach(
    // 			  function(entry)
    // 			  {
    // 			      console.log(entry);
    // 			      items.push( '<p>'+ entry +'</p>'  );
    // 			  }

    // 		      );

    // 		      // console.log(items);
    // 		      // $('#motors').html('<p> Motors: ' + data + '</p>');
    // 		      $('#motors').html(items);
    // 		  })


    // 	    .fail(
    // 		function(d, textStatus, error) {
    // 		    $('#fail').html('<p> getJSON failed, status: ' + textStatus + ', error: '+error+'</p>');

    // 		})
    // 	    // .always(function() {
    // 	    // 	$('#always').html('<p> Always </p>');
    // 	    // });

    // });
});
