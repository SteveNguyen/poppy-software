
import urllib2
import json
import time

import pypot.robot
import pypot.server

from poppytools.configuration.config import poppy_config


poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

host = 'localhot'
port = '8080'

server = pypot.server.HTTPServer(poppy, host, port)
# server = pypot.server.ZMQServer(poppy, host, port)
server.start()
# c = zmq.Context()
# s = c.socket(zmq.REQ)
# s.connect('tcp://{}:{}'.format(host, port))


while True:
    time.sleep(0.1)
