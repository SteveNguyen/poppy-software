import pypot.robot

import poppytools.primitive.basic as basic
from poppytools.configuration.config import poppy_config
import time


import pickle
import random

from explauto.utils.config import make_configuration
from explauto.sensorimotor_model.imle import ImleModel
from numpy import array

from math import *

U_MIN =- 10.0
U_MAX = 10.0

POS_MIN =- 20.0
POS_MAX = 20.0

SPEED_MIN =- 100.0
SPEED_MAX = 100.0

LOAD_MIN =- 100.0
LOAD_MAX = 100.0


# conf = make_configuration(m_mins = array([U_MIN]), m_maxs = array([U_MAX]), s_mins = array([POS_MIN,POS_MIN]), s_maxs = array([ POS_MAX, POS_MAX]))
conf = make_configuration(m_mins = array([U_MIN]), m_maxs = array([U_MAX]), s_mins = array([POS_MIN,POS_MIN, SPEED_MIN]), s_maxs = array([ POS_MAX, POS_MAX, SPEED_MAX]))
# conf = make_configuration(m_mins = array([U_MIN]), m_maxs = array([U_MAX]), s_mins = array([POS_MIN,SPEED_MIN, LOAD_MIN]), s_maxs = array([ POS_MAX, SPEED_MAX, LOAD_MAX]))
from explauto.sensorimotor_model.nearest_neighbor import NearestNeighbor
# model = NearestNeighbor(conf, sigma_ratio = 0.)

model = ImleModel(conf)

f = open('data.dat', 'r')
for l in f:
    l = l.replace('\n', '').split(' ')
    goal = float(l[0])
    present = float(l[1])
    past = float(l[2])
    # print goal, present, past
    model.update([goal], [present, past, present - past])

f.close()


# f = open('learnmodel.dat', 'r')
# model =  pickle.load(f)
# f.close()


poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')

poppy.stand_position.start()
poppy.stand_position.wait_to_stop()



poppy.abs_y.compliant = False
poppy.abs_y.torque_limit = 100

# pos = 0
# for i in range(1500):
#     if i % 10 == 0:
#         pos += 0.1
#         poppy.abs_y.goal_position = pos

#     print poppy.abs_y.goal_position, poppy.abs_y.present_position, poppy.abs_y.present_speed, poppy.abs_y.present_load

#     model.update([poppy.abs_y.goal_position], [poppy.abs_y.present_position, poppy.abs_y.present_speed, poppy.abs_y.present_load])
#     time.sleep(0.02)



# time.sleep(0.5)

# print poppy.abs_y.present_position

# test =- 2.0

# a = model.infer(conf.s_dims, conf.m_dims, [test,poppy.abs_y.present_position, poppy.abs_y.present_load])

# # print model.infer(conf.s_dims, conf.m_dims, [1.2,6.3, -5.0])
# # print model.infer(conf.s_dims, conf.m_dims, [7.1,- 2, - 8.0])

# print a,  [test,poppy.abs_y.present_position, poppy.abs_y.present_load]

# poppy.abs_y.goal_position = a
# time.sleep(1)

# print poppy.abs_y.present_position
# time.sleep(1)



# goal = 0.0

# STEP_MIN = 3000
# ginf = 0

# for i in range(1 * STEP_MIN):


#     pos = poppy.abs_y.present_position
#     load = poppy.abs_y.present_load
#     vel = poppy.abs_y.present_speed

#     lhip = poppy.l_hip_y.present_position
#     rhip = poppy.r_hip_y.present_position

#     print ginf, pos, goal

#     g = random.random() * (U_MAX - U_MIN) / 2.0 - U_MAX / 2.0

#     # print g
#     goal += (g / 2.0)

#     if goal > U_MAX:
#         goal -= abs(g / 5.0)
#     elif goal < U_MIN:
#         goal += abs(g / 5.0)



#     ginf = model.infer(conf.s_dims, conf.m_dims, [goal,pos])

#     poppy.abs_y.goal_position = ginf


#     time.sleep(0.02)

    # print poppy.abs_y.goal_position, poppy.abs_y.present_position, poppy.abs_y.present_speed, poppy.abs_y.present_load

    # model.update([poppy.abs_y.goal_position], [poppy.abs_y.present_position, poppy.abs_y.present_speed, poppy.abs_y.present_load])

    # s = '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (goal, poppy.abs_y.present_position, pos, load, vel, poppy.abs_y.present_speed, lhip, rhip, poppy.l_hip_y.present_position, poppy.r_hip_y.present_position, imu.tilt.x, imu.tilt.y,  imu.tilt.z, imu.acc.x, imu.acc.y, imu.acc.z, imu.gyro.x, imu.gyro.y, imu.gyro.z)
    # f.write(s)
    # print goal, poppy.abs_y.present_position, pos, load
    # model.update([goal], [poppy.abs_y.present_position, pos, load])
    # time.sleep(0.02)





t = 0.0
prev = 0
for i in range(500):

    g = 10 * sin(2.0 * pi * 1.0 * t)


    pos = poppy.abs_y.present_position
    ginf = model.infer(conf.s_dims, conf.m_dims, [g,pos, pos - prev])
    poppy.abs_y.goal_position = ginf
    prev = pos

    # print g, pos, ginf[0]
    print g, pos, ginf[0][0]

    t += 0.02
    time.sleep(0.02)


time.sleep(1.0)


poppy.stand_position.start()
poppy.stand_position.wait_to_stop()


# f = open('model.dat', 'w')
# pickle.dump(model, f)
# f.close()
