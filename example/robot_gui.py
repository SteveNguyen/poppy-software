#!/usr/bin/env python

import wx

from math import radians, sin, asin, pi
import time

TIMER_DT = 10

width = 325


class CkSlider():

    def __init__(self, parent, mini, maxi, idi, name):

        self.position = wx.Slider(parent, -1, 0, int(mini * 1000), int(maxi * 1000),
                                  wx.DefaultPosition, (150, -1), wx.SL_HORIZONTAL | wx.SL_AUTOTICKS)  # | wx.SL_LABELS

        #self.position.SetTickFreq(5, 1)
        self.enabled = wx.CheckBox(parent, idi, name + ":")
        self.enabled.SetValue(False)
        self.position.Disable()

    def Enable(self):
        self.position.Enable()

    def Disable(self):
        self.position.Disable()

    def setPosition(self, val):
        self.position.SetValue(int(val * 1000))

    def getPosition(self):
        return self.position.GetValue() / 1000.0


class servoSlider():

    def __init__(self, parent, min_angle, max_angle, motor, i):
        self.motor = motor

        self.position = wx.Slider(parent, -1, 0, int(min_angle * 100), int(max_angle * 100),
                                  wx.DefaultPosition, (150, -1), wx.SL_HORIZONTAL | wx.SL_AUTOTICKS)  # | wx.SL_LABELS

        #self.position.SetTickFreq(5, 1)
        self.enabled = wx.CheckBox(parent, i, motor.name + ":")
        self.enabled.SetValue(False)
        self.position.Disable()

    def Enable(self):
        self.position.Enable()

    def Disable(self):
        self.position.Disable()

    def setPosition(self, angle):
        self.position.SetValue(int(angle * 100))

    def getPosition(self):
        return self.position.GetValue() / 100.0


class controllerGUI(wx.Frame):
    TIMER_ID = 100

    def __init__(self, robot, parent, debug=False):
        wx.Frame.__init__(self, parent, -1, "Controller GUI",
                          style=wx.DEFAULT_FRAME_STYLE | (wx.RESIZE_BORDER | wx.MAXIMIZE_BOX))
        # sizer = wx.GridBagSizer(0,0)
        sizer = wx.BoxSizer(wx.VERTICAL)

        self.robot = robot

        servoSizer = wx.GridBagSizer(5, 5)
        # servoSizer = wx.GridSizer(30,5,1,1)
        # servoSizer = wx.BoxSizer(wx.VERTICAL)

        self.servos = dict()
        self.servos_pos = dict()
        self.labels = dict()
        self.labels_objs = dict()
        self.text_entry = dict()

        i = 0
        for m in self.robot.motors:

            min_angle = m.angle_limit[0]
            max_angle = m.angle_limit[1]

            init_angle = m.present_position

            # print m.name, min_angle, max_angle

            # create slider
            #s = servoSlider(self, min_angle, max_angle, name, i)
            s = servoSlider(self, min_angle, max_angle, m, i)
            s.setPosition(init_angle)

            servoSizer.Add(
                s.enabled, (i, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
            servoSizer.Add(
                s.position, (i, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)

            self.labels_objs[i] = wx.StaticText(
                self, label=str(s.getPosition()))

            self.text_entry[i] = wx.TextCtrl(
                self, i, size=(-1, -1), style=wx.TE_PROCESS_ENTER)

            self.labels[i] = ""

            servoSizer.Add(
                self.labels_objs[i], (i, 2), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
            servoSizer.Add(
                self.text_entry[i], (i, 4), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)

            self.servos[i] = s
            self.servos_pos[i] = init_angle
            i += 1

        # add everything
        sizer.Add(servoSizer, 1, wx.EXPAND | wx.ALL, 5)

        self.Bind(wx.EVT_CHECKBOX, self.enableSliders)

        self.Bind(wx.EVT_TEXT_ENTER, self.onTextEnter)
        # self.Bind(wx.EVT_SET_CURSOR, self.onTextInsert)
        #self.Bind(wx.EVT_COMMAND_LEFT_CLICK, self.onTextInsert)

        # timer for output
        self.timer = wx.Timer(self, self.TIMER_ID)
        self.timer.Start(TIMER_DT)
        wx.EVT_CLOSE(self, self.onClose)
        wx.EVT_TIMER(self, self.TIMER_ID, self.onTimer)

        self.SetSizerAndFit(sizer)
        self.Show(True)

    def onClose(self, event):

        self.timer.Stop()
        self.Destroy()

    def enableSliders(self, event):
        servo = event.GetId()

        if event.IsChecked():
            self.servos[servo].motor.compliant = False
            self.servos[servo].Enable()
            print self.servos[servo].motor.name, 'On'

        else:
            self.servos[servo].motor.compliant = True
            self.servos[servo].Disable()
            print self.servos[servo].motor.name, 'Off'

    def onTextEnter(self, event):
        s = event.GetId()

        if self.servos[s].enabled.IsChecked():
            self.servos[s].motor.goal_position = float(
                self.text_entry[s].GetValue())
            print self.servos[s].motor.name, float(self.text_entry[s].GetValue())
            time.sleep(0.1)

    def onTimer(self, event=None):

        for s in self.servos.keys():

            d = self.servos[s].motor.present_position
            self.labels_objs[s].SetLabel(str(d))

            if self.servos[s].enabled.IsChecked():

                p = self.servos[s].getPosition()
                self.labels_objs[s].SetLabel(
                    str(self.servos[s].motor.present_position))
                # print self.servos[s].motor.name, d#, l, float(l)
                self.servos[s].motor.goal_position = p  # float(l)

            else:
                self.servos[s].setPosition(
                    self.servos[s].motor.present_position)
                self.labels_objs[s].SetLabel(
                    str(self.servos[s].motor.present_position))


class SinGUI(wx.Frame):
    TIMER_ID = 100

    def __init__(self, primitive, parent, debug=False):
        wx.Frame.__init__(self, parent, -1, "Sinus GUI",
                          style=wx.DEFAULT_FRAME_STYLE | (wx.RESIZE_BORDER | wx.MAXIMIZE_BOX))
        # sizer = wx.GridBagSizer(0,0)
        sizer = wx.BoxSizer(wx.VERTICAL)

        self.primitive = primitive

        slSizer = wx.GridBagSizer(5, 5)
        # servoSizer = wx.GridSizer(30,5,1,1)
        # servoSizer = wx.BoxSizer(wx.VERTICAL)

        # self.servos = dict()
        # self.servos_pos = dict()

        self.sliders = dict()

        self.labels = dict()
        self.labels_objs = dict()
        self.text_entry = dict()

        self.amp = dict()
        self.freq = dict()

        # amp slider
        min_amp = 0.0
        max_amp = 0.04
        init_amp = self.primitive.amp
        sa = CkSlider(self, min_amp, max_amp, 0, 'Amp')
        sa.setPosition(init_amp)

        slSizer.Add(
            sa.enabled, (0, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        slSizer.Add(
            sa.position, (0, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)

        self.labels_objs[0] = wx.StaticText(self, label=str(sa.getPosition()))

        self.text_entry[0] = wx.TextCtrl(
            self, 0, size=(-1, -1), style=wx.TE_PROCESS_ENTER)

        self.labels[0] = ""

        slSizer.Add(
            self.labels_objs[0], (0, 2), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        slSizer.Add(
            self.text_entry[0], (0, 4), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)

        self.sliders[0] = sa

        # self.servos[i]=s
        # self.servos_pos[i]=init_angle

        min_freq = 0.0
        max_freq = 3.0
        init_freq = self.primitive.freq

        sf = CkSlider(self, min_freq, max_freq, 1, 'Freq')
        sf.setPosition(init_freq)

        slSizer.Add(
            sf.enabled, (1, 0), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        slSizer.Add(
            sf.position, (1, 1), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)

        self.labels_objs[1] = wx.StaticText(self, label=str(sf.getPosition()))

        self.text_entry[1] = wx.TextCtrl(
            self, 1, size=(-1, -1), style=wx.TE_PROCESS_ENTER)

        self.labels[1] = ""

        slSizer.Add(
            self.labels_objs[1], (1, 2), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        slSizer.Add(
            self.text_entry[1], (1, 4), wx.DefaultSpan, wx.ALIGN_CENTER_VERTICAL)
        self.sliders[1] = sf

        # add everything
        sizer.Add(slSizer, 1, wx.EXPAND | wx.ALL, 5)

        self.Bind(wx.EVT_CHECKBOX, self.enableSliders)
        self.Bind(wx.EVT_TEXT_ENTER, self.onTextEnter)
        # self.Bind(wx.EVT_SET_CURSOR, self.onTextInsert)
        #self.Bind(wx.EVT_COMMAND_LEFT_CLICK, self.onTextInsert)

        # timer for output
        self.timer = wx.Timer(self, self.TIMER_ID)
        self.timer.Start(TIMER_DT)
        wx.EVT_CLOSE(self, self.onClose)
        wx.EVT_TIMER(self, self.TIMER_ID, self.onTimer)

        self.SetSizerAndFit(sizer)
        self.Show(True)

    def onClose(self, event):
        self.primitive.stop()
        self.timer.Stop()
        self.Destroy()

    def enableSliders(self, event):
        sl = event.GetId()

        if event.IsChecked():
            # self.servos[servo].motor.compliant = False
            self.sliders[sl].Enable()
            # print self.servos[servo].motor.name, 'On'
            # print sl, 'Checked TODO'

        else:
            # print sl, 'Unchecked TODO'

            # self.servos[servo].motor.compliant = True
            # self.servos[servo].Disable()
            self.sliders[sl].Disable()
            # print self.servos[servo].motor.name, 'Off'

    def onTextEnter(self, event):
        s = event.GetId()

        # if self.servos[s].enabled.IsChecked():
        #     self.servos[s].motor.goal_position = float(self.text_entry[s].GetValue())
        #     print self.servos[s].motor.name, float(self.text_entry[s].GetValue())
        #     time.sleep(0.1)

        if self.sliders[s].enabled.IsChecked():
            if s == 0:
                self.primitive.SetAmp(float(self.text_entry[s].GetValue()))
                self.sliders[0].setPosition(
                    float(self.text_entry[s].GetValue()))
                print 'Amp', float(self.text_entry[s].GetValue())
            elif s == 1:
                self.primitive.SetFreq(float(self.text_entry[s].GetValue()))
                self.sliders[1].setPosition(
                    float(self.text_entry[s].GetValue()))
                print 'Freq', float(self.text_entry[s].GetValue())

    def onTimer(self, event=None):

        a = self.primitive.GetAmp()
        f = self.primitive.GetFreq()
        self.labels_objs[0].SetLabel(str(a))
        self.labels_objs[1].SetLabel(str(f))

        if self.sliders[0].enabled.IsChecked():  # amp
            amp = self.sliders[0].getPosition()
            # print 'amp', amp
            self.primitive.SetAmp(amp)
        else:
            self.sliders[0].setPosition(a)

        if self.sliders[1].enabled.IsChecked():  # freq
            freq = self.sliders[1].getPosition()
            # print 'freq', freq

            if freq != self.primitive.GetFreq():
                v = self.primitive.val
                t = asin(v) / (2.0 * pi * freq * self.primitive.GetAmp())
                self.primitive.SetFreq(freq)
                self.primitive.t = t
        else:
            self.sliders[1].setPosition(f)
