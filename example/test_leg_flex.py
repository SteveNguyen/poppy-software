#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config


import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config


# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()



duration = 0.1
up = 0.06

hip, knee, ankle = kinematics_dev.leg_up(up)

# print np.degrees(hip), np.degrees(knee), np.degrees(ankle)

# poppy.r_ankle_x.torque_limit = 3
# poppy.l_ankle_x.torque_limit = 3


# poppy.attach_primitive(kinematics.SagitallHipMotion(poppy), 'hip_motion')

# poppy.hip_motion.mouv_theta( 0)
# poppy.hip_motion.start()
# poppy.hip_motion.wait_to_stop()





#flexion
fhip, fknee, fankle = kinematics_dev.leg_up(0.02)


poppy.goto_position({'l_hip_y': np.degrees( - fhip),
                     'l_knee_y': np.degrees( - fknee),
                     'l_ankle_y': np.degrees(fankle),
                     'r_hip_y': np.degrees( - fhip) ,
                     'r_knee_y': np.degrees( - fknee),
                     'r_ankle_y': np.degrees(fankle),
                     'abs_y': 15.0
                 },
                    3,
                    wait=True)



time.sleep(2.0)

poppy.goto_position({'l_hip_y': np.degrees( - hip),
                        'l_knee_y': np.degrees( - knee),
                     'l_ankle_y': np.degrees(ankle),
                     'r_hip_x':- 15.0
                 },
                        duration,
                        wait=True)


poppy.goto_position({'l_hip_y': np.degrees( - fhip),
                        'l_knee_y': np.degrees( - fknee),
                        'l_ankle_y': np.degrees(fankle)},
                        duration,
                        wait=True)


time.sleep(2.0)



# poppy.goto_position({'l_hip_y': np.degrees( - hip),
#                         'l_knee_y': np.degrees( - knee),
#                         'l_ankle_y': np.degrees(ankle)},
#                         duration,
#                         wait=True)


# poppy.goto_position({'l_hip_y': np.degrees(0),
#                         'l_knee_y': np.degrees(0),
#                         'l_ankle_y': np.degrees(0)},
#                         duration,
#                         wait=True)


# poppy.goto_position({'r_hip_y': np.degrees( - hip),
#                         'r_knee_y': np.degrees( - knee),
#                         'r_ankle_y': np.degrees(ankle)},
#                         duration,
#                         wait=True)


# poppy.goto_position({'r_hip_y': np.degrees(0),
#                         'r_knee_y': np.degrees(0),
#                         'r_ankle_y': np.degrees(0)},
#                         duration,
#                         wait=True)


time.sleep(3.0)
poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()
