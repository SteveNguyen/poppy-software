#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_quad.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Thursday, May 15 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################


import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure

import poppytools.utils.min_jerk as min_jerk

from poppytools.primitive.interaction import ArmsCompliant, ArmsCopyMotion, SmartCompliance,  ArmsTurnCompliant

from poppytools.configuration.config import poppy_config

import robot_gui

import time

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

poppy.compliant = False
poppy.power_up()

poppy.attach_primitive(basic.QuadPosition(poppy), 'quad_position')
poppy.quad_position.start()
poppy.quad_position.wait_to_stop()


# time.sleep(10)
# poppy.compliant = True
time.sleep(1)

# poppy.goto_position({'abs_z': 10}, 3, wait = True)
# time.sleep(1)
# poppy.goto_position({'abs_z': 0}, 3, wait = True)
# time.sleep(1)

# poppy.goto_position({'r_hip_z':- 10}, 3, wait = True)
# time.sleep(1)
# poppy.goto_position({'r_hip_z': 0}, 3, wait = True)
# time.sleep(1)


# poppy.goto_position({'l_hip_y':-90}, 1, wait = True)
# time.sleep(1)
# poppy.goto_position({'l_hip_y':- 70}, 1, wait = True)
# time.sleep(1)

poppy.bust_y.compliant = True
poppy.l_hip_y.compliant = True
poppy.r_hip_y.compliant = True
time.sleep(1)
