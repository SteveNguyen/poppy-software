#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure
import poppytools.sensor.inertial_unit as imu

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config


import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config
import math

import os

# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



f = open('pendule_c.dat', 'w')
fd = open('pendule_d.dat', 'w')

poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()


lfoot = pressure.FootPressure('/dev/poppy_lfoot')
rfoot = pressure.FootPressure('/dev/poppy_rfoot')

imu = imu.Imu('/dev/poppy_imu')
imu.start()

lfoot.start()
rfoot.start()

time.sleep(3)


# poppy.power_up()

# poppy.l_ankle_x.pid = (0.3, 0, 0)
# poppy.r_ankle_x.pid = (0.3, 0, 0)



# c, t1, t2, t3, t4, t5 = kinematics_dev.get_para( 0.025 )#+ 0.075)

# print c, np.degrees(t1), np.degrees(t2), np.degrees(t3 - np.pi / 2.0), np.degrees(t4 - np.pi / 2.0), np.degrees(t5)

# lsum = []
# rsum = []

# print "init"
# for i in range(50):

#     lsum.append(lfoot.pressor_values[1] + lfoot.pressor_values[2] + lfoot.pressor_values[3] + lfoot.pressor_values[4])
#     rsum.append(rfoot.pressor_values[1] + rfoot.pressor_values[2] + rfoot.pressor_values[3] + rfoot.pressor_values[4])
#     time.sleep(0.05)


# lsum = np.nansum(lsum) / (np.count_nonzero(~np.isnan(lsum)))
# rsum = np.nansum(rsum) / (np.count_nonzero(~np.isnan(rsum)))



# print "init done ", lsum, rsum

# for i in range(500):
#     print (lfoot.pressor_values[1] + lfoot.pressor_values[2] + lfoot.pressor_values[3] + lfoot.pressor_values[4]) / lsum, (rfoot.pressor_values[1] + rfoot.pressor_values[2] + rfoot.pressor_values[3] + rfoot.pressor_values[4]) / rsum, ((lfoot.pressor_values[1] + lfoot.pressor_values[2]) - (lfoot.pressor_values[3] + lfoot.pressor_values[4])) / lsum, ((rfoot.pressor_values[1] + rfoot.pressor_values[2]) - (rfoot.pressor_values[3] + rfoot.pressor_values[4])) / rsum, ((lfoot.pressor_values[1] + lfoot.pressor_values[2]) - (lfoot.pressor_values[3] + lfoot.pressor_values[4])) / lsum + ((rfoot.pressor_values[1] + rfoot.pressor_values[2]) - (rfoot.pressor_values[3] + rfoot.pressor_values[4])) / rsum
#     # print lfoot.pressor_values[1], lfoot.pressor_values[2], lfoot.pressor_values[3], lfoot.pressor_values[4], rfoot.pressor_values[1], rfoot.pressor_values[2], rfoot.pressor_values[3], rfoot.pressor_values[4]
#     time.sleep(0.05)


# stop

# # poppy.power_up()
# poppy.goto_position({  'l_ankle_x':np.degrees(t2 - np.pi / 2.0),
#                        'r_ankle_x':np.degrees(t1 - np.pi / 2.0),
#                        'l_hip_x': np.degrees(t4 - np.pi / 2.0) - 4,
#                        'r_hip_x': np.degrees(np.pi / 2.0 - t3) + 4,
#                        'abs_x': np.degrees( - t5)
#                    },
#                         2.0,
#                     wait=False)

# for i in range(200):
#     # print lfoot.pressor_values[1] + lfoot.pressor_values[2] + lfoot.pressor_values[3] + lfoot.pressor_values[4], rfoot.pressor_values[1] + rfoot.pressor_values[2] + rfoot.pressor_values[3] + rfoot.pressor_values[4]
#     print lfoot.pressor_values[1], lfoot.pressor_values[2], lfoot.pressor_values[3], lfoot.pressor_values[4], rfoot.pressor_values[1], rfoot.pressor_values[2], rfoot.pressor_values[3], rfoot.pressor_values[4]
#     time.sleep(0.05)




# for i in range(1000):
#     print imu.acc.x, imu.acc.y,imu.acc.z, imu.gyro.x,imu.gyro.y, imu.gyro.z #imu.tilt, imu.magneto
#     time.sleep(0.01)


poppy.power_up()

poppy.l_ankle_x.pid = (0.3, 0, 0)
poppy.r_ankle_x.pid = (0.3, 0, 0)

poppy.abs_x.pid = (10, 0, 0)
poppy.abs_y.pid = (10, 0, 0)

poppy.l_ankle_y.pid = (10, 0, 0)
poppy.r_ankle_y.pid = (10, 0, 0)






poppy.attach_primitive(kinematics_dev.SlowPID(poppy, 50, {'l_ankle_y': 1.0, 'r_ankle_y': 1.0, 'abs_y': 5.0}), 'spid')
poppy.spid.start()


prevtime = time.time()

for i in range(100):
    pitch=random.uniform(0.0,1.0)*2.5 + 0.5
    roll = random.uniform( - 1.0, 1.0) * 0.02


    # poppy.goto_position({'l_ankle_y': pitch,
    #                      'r_ankle_y': pitch},
    #                     2.0,
    #                     wait = False)


    poppy.spid.new_goal('l_ankle_y', pitch)
    poppy.spid.new_goal('r_ankle_y', pitch)

    kinematics_dev.move_para(poppy, roll, 2.0, False)

    print roll, pitch

    for t in range(150):
        t = time.time()
        #todo data
        print i, pitch, roll
        s = '{} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {}\n'.format(t, (t - prevtime), pitch, roll, poppy.l_ankle_y.present_position, poppy.r_ankle_y.present_position, lfoot.pressor_values[1], lfoot.pressor_values[2], lfoot.pressor_values[3], lfoot.pressor_values[4], rfoot.pressor_values[1], rfoot.pressor_values[2], rfoot.pressor_values[3], rfoot.pressor_values[4], imu.acc.x, imu.acc.y, imu.acc.z, imu.gyro.x, imu.gyro.y, imu.gyro.z, imu.tilt.x, imu.tilt.y, imu.tilt.z, poppy.l_ankle_x.present_position, poppy.r_ankle_x.present_position, poppy.abs_x.present_position, poppy.abs_y.present_position)


        # #re
        # poppy.goto_position({'l_ankle_y': pitch,
        #                      'r_ankle_y': pitch},
        #                     2.0,
        #                     wait = False)

        # kinematics_dev.move_para(poppy, roll, 2.0, False)

        print s
        # print "DATA"
        f.write(s)
        time.sleep(0.05)
        prevtime = t

    fd.write(s)


f.close()
fd.close()

time.sleep(3.0)
poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()
