#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure

import poppytools.utils.min_jerk as min_jerk

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config


import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config
import math

# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()

# poppy.power_up()
# poppy.compliant = True

lfoot = pressure.FootPressure('/dev/poppy_lfoot')
rfoot = pressure.FootPressure('/dev/poppy_rfoot')

lfoot.start()
rfoot.start()













# for i in range(10000):
#     print kinematics_dev.get_db(poppy)
#     time.sleep(0.02)
# stop


poppy.attach_primitive(kinematics_dev.TrackComSimple(poppy), 'com')
poppy.com.start()

time.sleep(1)

pos = poppy.l_hip_y.present_position
goal = 0.02
print poppy.com.com_sagital

while abs(poppy.com.com_sagital[0] - goal) > 0.005:

    pos +=- 0.5 * (goal - poppy.com.com_sagital[0])
    print poppy.com.com_sagital, pos
    # poppy.goto_position({'l_hip_y': pos, 'r_hip_y': pos}, 0.5, wait = True)
    poppy.l_hip_y.goal_position = pos
    poppy.r_hip_y.goal_position = pos
    time.sleep(0.1)


# time.sleep(30)
