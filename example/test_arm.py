#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure
import poppytools.sensor.inertial_unit as inertial

import poppytools.utils.min_jerk as min_jerk

from poppytools.primitive.interaction import ArmsCompliant, ArmsCopyMotion, SmartCompliance,  ArmsTurnCompliant

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config


import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config
import math

# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



# poppy.attach_primitive(basic.SafeStandPosition(poppy), 'stand_position')
# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()




# poppy.power_up()

poppy.l_shoulder_y.compliant = True
poppy.l_elbow_y.compliant = True

time.sleep(3)

poppy.l_shoulder_y.compliant = False
poppy.l_elbow_y.compliant = False

time.sleep(3)

for i in range (10):

    a, b = kinematics_dev.two_links_ik_2d_cart([0.2], [0], 0.15, 0.15)
    print np.degrees(a), np.degrees(b)

    # poppy.l_shoulder_y.goal_position = 180.0 - np.degrees(a)
    # poppy.l_elbow_y.goal_position =- np.degrees(b)

    poppy.l_elbow_y.goal_position =10

    time.sleep(0.1)
