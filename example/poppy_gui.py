import pypot.robot

import poppytools.primitive.basic as basic
from poppytools.configuration.config import poppy_config

import wx

import robot_gui

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()




poppy.power_up()
poppy.compliant = True

# app = wx.PySimpleApp()
app = wx.App(False)
frame = robot_gui.controllerGUI(poppy, None, True)
app.MainLoop()



# poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()
