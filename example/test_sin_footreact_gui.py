#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure
import poppytools.sensor.inertial_unit as inertial

import poppytools.utils.min_jerk as min_jerk

from poppytools.primitive.interaction import ArmsCompliant, ArmsCopyMotion, SmartCompliance,  ArmsTurnCompliant

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config

import robot_gui

import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config
import math
import wx

# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



poppy.attach_primitive(basic.SafeStandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()



lfoot = pressure.FootPressure('/dev/poppy_lfoot')
rfoot = pressure.FootPressure('/dev/poppy_rfoot')

lfoot.start()
rfoot.start()

# lfoot = None
# rfoot = None

imu = inertial.Imu('/dev/poppy_imu')
imu.start()

poppy.power_up()


poppy.attach_primitive(kinematics_dev.SlowPID(poppy, 50, {'abs_y': 1.0}), 'spid')
# poppy.spid.start()



#allow for a compliance of the ankle x
poppy.l_ankle_x.pid = (0.25, 0, 0)
poppy.r_ankle_x.pid = (0.25, 0, 0)

#just to be sure we are in a straight position

poppy.attach_primitive(min_jerk.MoveGprojMJ(poppy,0.0, 2.0), 'gprojmj')
poppy.gprojmj.start()
poppy.gprojmj.wait_to_stop()


#higher gain for ankle y
poppy.l_ankle_y.pid = (10, 2, 0)
poppy.r_ankle_y.pid = (10, 2, 0)

# poppy.l_hip_x.pid = (20, 0, 0)
# poppy.r_hip_x.pid = (20, 0, 0)

poppy.abs_x.pid = (10, 0, 0)
poppy.abs_y.pid = (10, 0, 0)


# poppy.abs_y.goal_position = 1.0
# poppy.l_ankle_y.goal_position = 0.0
# poppy.r_ankle_y.goal_position = 0.0


poppy.attach_primitive(kinematics_dev.FastPID(poppy, 50, {'l_ankle_y': 1.0}, kp = 5), 'lay')
poppy.attach_primitive(kinematics_dev.FastPID(poppy, 50, {'r_ankle_y': 1.0}, kp = 5), 'ray')
poppy.attach_primitive(kinematics_dev.FastPID(poppy, 50, {'abs_y': 3.0}, kp = 5), 'aby')

# poppy.lay.start()
# poppy.ray.start()
# poppy.aby.start()



# poppy.attach_primitive(kinematics_dev.FastPID(poppy, 50, {'l_hip_x': 0.0}, kp = 10), 'lhx')
# poppy.attach_primitive(kinematics_dev.FastPID(poppy, 50, {'r_hip_x': 0.0}, kp = 10), 'rhx')

# poppy.lhx.start()
# poppy.rhx.start()


poppy.attach_primitive(kinematics_dev.ControlFeetDist(poppy, strict=True),  'feetdist')
poppy.feetdist.start()

# time.sleep(30)
# stop


#find the equilibrium point
# poppy.attach_primitive(kinematics_dev.CompensateFoot(poppy, 50, 3.0, continuous = False), 'Compensate')
# poppy.Compensate.start()
# poppy.Compensate.wait_to_stop()

#attach primitives to move the legs

# poppy.attach_primitive(kinematics_dev.StabilizeTrunkY(poppy, imu, offset=5, kp=0.02, kd = 0), 'staby')
# poppy.staby.start()

#ok we are ready
min_jerk.yes_no(poppy, 'y', wait = True)
time.sleep(1.0)


poppy.attach_primitive(min_jerk.FeetReflex(poppy, lfoot, rfoot), 'FReflex')
poppy.FReflex.start()


time.sleep(0.5)
# poppy.attach_primitive(kinematics_dev.MoveGprojSin(poppy, 0, 1.0), 'gsin')

app = wx.PySimpleApp()
frame = robot_gui.SinGUI(poppy.gsin, None, True)
app.MainLoop()



# time.sleep(10)

# poppy.gsin.stop()


# time.sleep(3.0)
poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()
