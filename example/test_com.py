#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config
import poppytools.utils.min_jerk as min_jerk

import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config
import math

# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()


lfoot = pressure.FootPressure('/dev/poppy_lfoot')
rfoot = pressure.FootPressure('/dev/poppy_rfoot')

lfoot.start()
rfoot.start()


poppy.power_up()



poppy.goto_position({  'abs_y': 10
                   },
                    3.0,
                    wait=True)


# poppy.compliant = True

# poppy.l_ankle_x.pid = (0.3, 0, 0)
# poppy.r_ankle_x.pid = (0.3, 0, 0)

# poppy.abs_y.compliant = True

for i in range(1000):
    kinematics_dev.get_com_fk(poppy)
    time.sleep(0.02)


# min_jerk.yes_no(poppy, 'y', wait = True)
# time.sleep(1.0)
# min_jerk.yes_no(poppy, 'n', wait = True)




# time.sleep(3.0)
# poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()
