#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic

import poppytools.primitive.interaction as interaction

import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.utils.min_jerk as min_jerk

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config


import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config


# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# # poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()



poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()



duration = 0.1
up = 0.03

# hip, knee, ankle = kinematics_dev.leg_up(up)


mj1 = min_jerk.MJTraj(0, up, duration)
mj2 = min_jerk.MJTraj(up, 0, duration)

# mj3 = mj1 + mj2


# ts = np.arange(0, 10.0, .02)

# mm = mj3.getGen()

# # print mm(ts)
# t = 0.0
# for i in range(50):
#     print mm([t])
#     t += 0.02

# stop

poppy.power_up()
poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "left", mj1), 'mjleftup')
poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "left", mj2), 'mjleftdown')

poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "right", mj1), 'mjrightup')
poppy.attach_primitive(min_jerk.MJLegs1D(poppy, 50, "right", mj2), 'mjrightdown')

poppy.abs_y.goal_position = 5

poppy.l_hip_x.goal_position = 5.0
poppy.r_hip_x.goal_position =- 5.0

poppy.l_shoulder_x.goal_position = 35.0
poppy.r_shoulder_x.goal_position =- 35.0

poppy.l_shoulder_y.goal_position =- 90.0
poppy.r_shoulder_y.goal_position =- 90.0

poppy.l_ankle_x.compliant = True
poppy.r_ankle_x.compliant = True


# poppy.attach_primitive(interaction.ArmsCompliant(poppy, 50), 'compliant')
# poppy.compliant.start()

for i in range(50):

    poppy.mjleftup.start()
    poppy.mjleftup.wait_to_stop()

    poppy.mjleftdown.start()
    poppy.mjleftdown.wait_to_stop()


    time.sleep(0.1)

    poppy.mjrightup.start()
    poppy.mjrightup.wait_to_stop()

    poppy.mjrightdown.start()
    poppy.mjrightdown.wait_to_stop()




time.sleep(3)


poppy.stand_position.start()
poppy.stand_position.wait_to_stop()

# print np.degrees(hip), np.degrees(knee), np.degrees(ankle)

# poppy.r_ankle_x.torque_limit = 3
# poppy.l_ankle_x.torque_limit = 3


# poppy.attach_primitive(kinematics.SagitallHipMotion(poppy), 'hip_motion')

# poppy.hip_motion.mouv_theta( 0)
# poppy.hip_motion.start()
# poppy.hip_motion.wait_to_stop()





#flexion
# fhip, fknee, fankle = kinematics_dev.leg_up(0.02)


# poppy.goto_position({'l_hip_y': np.degrees( - fhip),
#                      'l_knee_y': np.degrees( - fknee),
#                      'l_ankle_y': np.degrees(fankle),
#                      'r_hip_y': np.degrees( - fhip) ,
#                      'r_knee_y': np.degrees( - fknee),
#                      'r_ankle_y': np.degrees(fankle),
#                      'abs_y': 15.0
#                  },
#                     3,
#                     wait=True)



# time.sleep(2.0)

# poppy.goto_position({'l_hip_y': np.degrees( - hip),
#                         'l_knee_y': np.degrees( - knee),
#                      'l_ankle_y': np.degrees(ankle),
#                      'r_hip_x':- 15.0
#                  },
#                         duration,
#                         wait=True)


# poppy.goto_position({'l_hip_y': np.degrees( - fhip),
#                         'l_knee_y': np.degrees( - fknee),
#                         'l_ankle_y': np.degrees(fankle)},
#                         duration,
#                         wait=True)


# time.sleep(2.0)


# poppy.l_ankle_x.compliant = True
# poppy.r_ankle_x.compliant = True

# poppy.power_up()
# poppy.l_shoulder_x.goal_position = 90
# poppy.r_shoulder_x.goal_position =- 90

# time.sleep(1.0)

# for i in range(10):


#     poppy.goto_position({'l_hip_y': np.degrees( - hip),
#                          'l_knee_y': np.degrees( - knee),
#                          'l_ankle_y': np.degrees(ankle)
#                      },
#                         duration,
#                         wait=True)


#     poppy.goto_position({'l_hip_y': np.degrees(0),
#                          'l_knee_y': np.degrees(0),
#                          'l_ankle_y': np.degrees(0)
#                      },
#                         duration,
#                         wait=True)
#     time.sleep(0.5)

#     poppy.goto_position({'r_hip_y': np.degrees( - hip),
#                         'r_knee_y': np.degrees( - knee),
#                         'r_ankle_y': np.degrees(ankle)},
#                         duration,
#                         wait=True)


#     poppy.goto_position({'r_hip_y': np.degrees(0),
#                          'r_knee_y': np.degrees(0),
#                          'r_ankle_y': np.degrees(0)},
#                         duration,
#                         wait=True)
#     time.sleep(0.5)




# poppy.goto_position({'l_hip_y': np.degrees( - hip),
#                         'l_knee_y': np.degrees( - knee),
#                         'l_ankle_y': np.degrees(ankle)},
#                         duration,
#                         wait=True)


# poppy.goto_position({'l_hip_y': np.degrees(0),
#                         'l_knee_y': np.degrees(0),
#                         'l_ankle_y': np.degrees(0)},
#                         duration,
#                         wait=True)


# poppy.goto_position({'r_hip_y': np.degrees( - hip),
#                         'r_knee_y': np.degrees( - knee),
#                         'r_ankle_y': np.degrees(ankle)},
#                         duration,
#                         wait=True)


# poppy.goto_position({'r_hip_y': np.degrees(0),
#                         'r_knee_y': np.degrees(0),
#                         'r_ankle_y': np.degrees(0)},
#                         duration,
#                         wait=True)


# time.sleep(3.0)
# poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()
