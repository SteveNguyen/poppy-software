#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_ik.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Wednesday, February 26 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import pypot.robot

# import poppy

import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev

import poppytools.sensor.pressure as pressure
import poppytools.sensor.inertial_unit as inertial
import poppytools.utils.min_jerk as min_jerk

from poppytools.primitive.interaction import ArmsCompliant, ArmsCopyMotion, SmartCompliance,  ArmsTurnCompliant

# import kinematics

# import poppy_config as config

from poppytools.configuration.config import poppy_config


import time
# from poppytools.configuration.config import poppy_config
# import poppytools.configuration.poppy_config_generator as config
import random

import numpy as np
# print poppy_config
import math

# poppy_config=config.poppy_config
# print poppy_config

poppy = pypot.robot.from_config(poppy_config)
poppy.start_sync()

# poppy.attach_primitive(custom_primitives.StandPosition(poppy), 'stand_position')
# poppy.attach_primitive(custom_primitives.TorqueOff(poppy), 'off')

# poppy.stand_position.start()
# poppy.stand_position.wait_to_stop()


poppy.attach_primitive(basic.SafeStandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()


# lfoot = pressure.FootPressure('/dev/poppy_lfoot')
# rfoot = pressure.FootPressure('/dev/poppy_rfoot')

# lfoot.start()
# rfoot.start()


# imu = inertial.Imu('/dev/poppy_imu')
# imu.start()


poppy.power_up()


# Slow PID on the abs to keep it straight
# poppy.attach_primitive(kinematics_dev.SlowPID(poppy, 50, {'abs_y': 0.0}), 'spid')
# poppy.spid.start()


# allow for a compliance of the ankle x #FIXME
poppy.l_ankle_x.pid = (0.25, 0, 0)
poppy.r_ankle_x.pid = (0.25, 0, 0)

# just to be sure we are in a straight position

# poppy.attach_primitive(min_jerk.MoveGprojMJ(poppy, 0.0, 2.0), 'gprojmj')
# poppy.gprojmj.start()
# poppy.gprojmj.wait_to_stop()


# find the equilibrium point
# poppy.attach_primitive(kinematics_dev.CompensateFoot(poppy, 50, 3.0, continuous = False), 'Compensate')
# poppy.Compensate.start()
# poppy.Compensate.wait_to_stop()

# attach primitives to move the legs


# ok we are ready
min_jerk.yes_no(poppy, 'y', wait=True)
time.sleep(1.0)


# X_COM = 0.015
# X_COM = 0.028 #pas mal sans footreact
X_COM = 0.024


# poppy.attach_primitive(SmartCompliance(poppy, poppy.arms, 50), 'arms_compliant')
# poppy.arms_compliant.start()


# poppy.attach_primitive(kinematics_dev.FootReact(poppy, lfoot, rfoot), 'footreact')
# poppy.footreact.start()

# poppy.attach_primitive(kinematics_dev.MoveGprojSin(poppy, X_COM, 1.2),
# 'gsin') #pas mal sans footreact

# poppy.attach_primitive(
#     kinematics_dev.MoveGprojSin(poppy, X_COM, 1.116), 'gsin')


poppy.abs_y.goal_position = 1.0
poppy.l_ankle_y.goal_position = 0.0
poppy.r_ankle_y.goal_position = 0.0


poppy.r_ankle_y.pid = (60, 30, 0)
poppy.l_ankle_y.pid = (20, 20, 0)

poppy.r_hip_x.pid = (60, 60, 0)
poppy.l_hip_x.pid = (60, 60, 0)

poppy.r_hip_y.pid = (60, 60, 0)
poppy.l_hip_y.pid = (60, 60, 0)

poppy.abs_y.pid = (60, 60, 0)
poppy.abs_x.pid = (50, 50, 0)


poppy.attach_primitive(
    kinematics_dev.MoveGprojSinHip(poppy, X_COM, 1.116,  arms=False), 'gsin')
poppy.gsin.start()


# poppy.attach_primitive(kinematics_dev.CtrlComSimple(poppy, 0.01), 'comctrl')
# poppy.comctrl.start()


# poppy.attach_primitive(kinematics_dev.StabilizeTrunkY(poppy, imu, 5), 'staby')
# poppy.staby.start()


import pygame
screen = pygame.display.set_mode((150, 150))  # for the focus


abs_y_ctrl = 0.0

while True:
    event = pygame.event.wait()

    if event.type in (pygame.KEYDOWN, pygame.KEYUP):
        key = pygame.key.name(event.key)

    if event.type == pygame.KEYDOWN:

        if event.key == pygame.K_UP:
            # poppy.power_up()
            poppy.gsin.move_forward = True
            print "Forward"

        elif event.key == pygame.K_LEFT:
            # poppy.power_up()
            poppy.gsin.turn = 'left'
            print "Turn left"

        elif event.key == pygame.K_RIGHT:
            # poppy.power_up()
            poppy.gsin.turn = 'right'
            print "Turn right"

        elif event.key == pygame.K_ESCAPE:
            pygame.quit()
            raise KeyboardInterrupt

    elif event.type == pygame.KEYUP:

        if event.key == pygame.K_UP:
            poppy.gsin.move_forward = False
            print "Stop"

        elif event.key == pygame.K_LEFT:
            poppy.gsin.turn = False
            print "Stop turning left"

        elif event.key == pygame.K_RIGHT:
            poppy.gsin.turn = False
            print "Stop turning right"


# raz
poppy.gprojmj.add(0.0, 2.0)
poppy.gprojmj.start()
poppy.gprojmj.wait_to_stop()


# time.sleep(3.0)
poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()
