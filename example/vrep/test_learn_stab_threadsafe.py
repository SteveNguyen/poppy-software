#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_vrep.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Thursday, July  3 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import random
import numpy as np
import math
import time as systime
import pypot.utils.pypot_time as time
import sys
# sys.path.append('/home/steve/divers/V-REP_PRO_EDU_V3_1_2_64_Linux/programming/remoteApiBindings/python/python/')

# sys.path.append(
# '/home/steve/divers/V-REP/programming/remoteApiBindings/python/python/')


sys.path.append('/home/steve/Project/Repo/poppy-software/example/')  # fixme

from pypot.vrep import from_vrep
import pypot.vrep
# import vrep
import pypot.vrep.remoteApiBindings.vrep as vrep

from poppytools.configuration.config import poppy_config

import poppytools.utils.vrep_tools as vrep_tools


import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev
import poppytools.utils.min_jerk as min_jerk

import pypot


# class MoveFootGround(pypot.primitive.Primitive):
#     def __init__(self, robot):
#         pypot.primitive.Primitive.__init__(self, robot)

#         self.lpos = 0.0
#         self.rpos = 0.0

#         self.up = 0.05
#         self.up_duration = 0.15
#         self.down_duration = 0.15

#         self.mj1 = min_jerk.MJTraj(0, self.up, self.up_duration)
#         self.mj2 = min_jerk.MJTraj(self.up, 0, self.down_duration)


#         self.robot.attach_primitive(min_jerk.MJLegs1D,(self.robot, "left", self.mj1, 0), 'mjleftup')
# self.robot.attach_primitive(min_jerk.MJLegs1D(self.robot, "left",
# self.mj2, 0), 'mjleftdown')

#         self.robot.attach_primitive(min_jerk.MJLegs1D(self.robot, "right", self.mj1, 0), 'mjrightup')
# self.robot.attach_primitive(min_jerk.MJLegs1D(self.robot, "right",
# self.mj2, 0), 'mjrightdown')


class LearnStab():

    def __init__(self,  robot, client_id, obj_h, dummies_h):

        self.fgoals = open('stab_goals.dat', 'w+')
        self.fdyn = open('stab_dyn.dat', 'w+')

        self.fdyn_mid = open('stab_dyn_mid.dat', 'w+')

        self.robot = robot
        self.state = 'right'
        self.goal_leg = 0

        self.duration = 0.25
        self.client_id = client_id
        self.up = 0.1

        self.vrepio = self.robot._controllers[0].io

        self.lfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
            'foot_left_visual'], obj_h['base_link_visual'], streaming=True)

        self.rfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
            'foot_right_visual'], obj_h['base_link_visual'], streaming=True)

        self.com = self.vrepio.call_remote_api('simxGetObjectPosition', dummies_h['CoM'], obj_h[
            'base_link_visual'], streaming=True)

        self.com_abs = self.vrepio.call_remote_api(
            'simxGetObjectPosition', dummies_h['CoM'], - 1, streaming=True)

        self.com_vel = self.vrepio.call_remote_api(
            'simxGetObjectVelocity', dummies_h['CoM'], streaming=True)

        self.lfoot = np.array(
            [- self.lfoot[0], - self.lfoot[1], self.lfoot[2]])
        self.rfoot = np.array(
            [- self.rfoot[0], - self.rfoot[1], self.rfoot[2]])
        self.com = np.array([- self.com[0], - self.com[1], self.com[2]])

        self.lfoot_pos_com = np.array(self.lfoot) - np.array(self.com)
        self.rfoot_pos_com = np.array(self.rfoot) - np.array(self.com)

        # self.lfoot_pos_com = np.array([ - self.lfoot[0],- self.lfoot[1], self.lfoot[2]])
        # self.rfoot_pos_com = np.array([ - self.rfoot[0],- self.rfoot[1],
        # self.rfoot[2]])

        # self.lfoot_vel_com = (np.array(self.lfoot_vel) - np.array(self.com_vel))
        # self.rfoot_vel_com = (np.array(self.rfoot_vel) -
        # np.array(self.com_vel))

        self.lfoot_vel_com = np.array([0, 0, 0])
        self.rfoot_vel_com = np.array([0, 0, 0])

        # self.robot.attach_primitive(min_jerk.MJLegs3DSym(self.robot, 'left'), 'lleg3d')
        # self.robot.attach_primitive(min_jerk.MJLegs3DSym(self.robot,
        # 'right'), 'rleg3d')

        self.robot.attach_primitive(
            min_jerk.MJLegsUp3D(self.robot, 'left', self.up), 'lleg3d_up')
        self.robot.attach_primitive(
            min_jerk.MJLegsUp3D(self.robot, 'right', self.up), 'rleg3d_up')

        self.robot.attach_primitive(
            min_jerk.MJLegsDown3D(self.robot, 'left', self.up), 'lleg3d_down')
        self.robot.attach_primitive(
            min_jerk.MJLegsDown3D(self.robot, 'right', self.up), 'rleg3d_down')

        # self.robot.attach_primitive(kinematics_dev.StabilizeTrunkYHipAnkle(
        #     poppy, imu, kp=1.0 / 100.0, kd=0.0), 'stab')
        # self.robot.stab.start()
        # self.robot.stab.wait_to_start()
        # self.robot.stab.state = self.state

        self.t0 = time.time()
        # time.sleep(0.1)

    def update(self):

        # self.lfoot = vrep.simxGetObjectPosition(client_id, obj_h['foot_left_visual'],- 1, vrep.simx_opmode_buffer)[1]
        # self.rfoot = vrep.simxGetObjectPosition(client_id, obj_h['foot_right_visual'],- 1, vrep.simx_opmode_buffer)[1]
        # self.com = vrep.simxGetObjectPosition(client_id, dummies_h['CoM'],-
        # 1, vrep.simx_opmode_buffer)[1]

        # self.lfoot_vel = vrep.simxGetObjectVelocity(client_id, obj_h['foot_left_visual'], vrep.simx_opmode_buffer)[1]
        # self.rfoot_vel = vrep.simxGetObjectVelocity(client_id, obj_h['foot_right_visual'], vrep.simx_opmode_buffer)[1]
        # self.com_vel = vrep.simxGetObjectVelocity(client_id,
        # dummies_h['CoM'], vrep.simx_opmode_buffer)[1]

        # lfoot = vrep.simxGetObjectPosition(client_id, obj_h['foot_left_visual'],obj_h['base_link_visual'], streaming=True)[1]
        # rfoot = vrep.simxGetObjectPosition(client_id,
        # obj_h['foot_right_visual'],obj_h['base_link_visual'],
        # streaming=True)[1]

        # self.lfoot = vrep.simxGetObjectPosition(client_id, obj_h[
        #                                         'foot_left_visual'], obj_h['base_link_visual'], vrep.simx_opmode_buffer)[1]
        # self.rfoot = vrep.simxGetObjectPosition(client_id, obj_h[
        #                                         'foot_right_visual'], obj_h['base_link_visual'], vrep.simx_opmode_buffer)[1]

        # self.com = vrep.simxGetObjectPosition(client_id, dummies_h['CoM'],-
        # 1, streaming=True)[1]
        # self.com = vrep.simxGetObjectPosition(
        # client_id, dummies_h['CoM'], obj_h['base_link_visual'],
        # vrep.simx_opmode_buffer)[1]

        # self.com_abs = vrep.simxGetObjectPosition(
        #     client_id, dummies_h['CoM'], - 1, vrep.simx_opmode_buffer)[1]

        self.lfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
            'foot_left_visual'], obj_h['base_link_visual'], streaming=True)

        self.rfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
            'foot_right_visual'], obj_h['base_link_visual'], streaming=True)

        self.com = self.vrepio.call_remote_api('simxGetObjectPosition', dummies_h['CoM'], obj_h[
            'base_link_visual'], streaming=True)

        self.com_abs = self.vrepio.call_remote_api(
            'simxGetObjectPosition', dummies_h['CoM'], - 1, streaming=True)

        self.com_vel = self.vrepio.call_remote_api(
            'simxGetObjectVelocity', dummies_h['CoM'], streaming=True)

        # print self.com_abs

        self.lfoot = np.array(
            [- self.lfoot[0], - self.lfoot[1], self.lfoot[2]])
        self.rfoot = np.array(
            [- self.rfoot[0], - self.rfoot[1], self.rfoot[2]])
        self.com = np.array([- self.com[0], - self.com[1], self.com[2]])

        # print self.com, self.com_vel, self.lfoot, self.rfoot
        # lfoot_pos_com = np.array( [ -  lfoot[0],- lfoot[1], lfoot[2]])
        # rfoot_pos_com = np.array( [- rfoot[0],- rfoot[1], rfoot[2]])

        lfoot_pos_com = np.array(self.lfoot) - np.array(self.com)
        rfoot_pos_com = np.array(self.rfoot) - np.array(self.com)

        dt = time.time() - self.t0
        self.t0 = time.time()

        if dt <= 0.0:
            self.lfoot_vel_com = np.array([0, 0, 0])
            self.rfoot_vel_com = np.array([0, 0, 0])
        else:

            self.lfoot_vel_com = (
                np.array(lfoot_pos_com) - np.array(self.lfoot_pos_com)) / dt
            self.rfoot_vel_com = (
                np.array(rfoot_pos_com) - np.array(self.rfoot_pos_com)) / dt

        self.lfoot_pos_com = lfoot_pos_com
        self.rfoot_pos_com = rfoot_pos_com

        # print self.lfoot_pos_com, self.rfoot_pos_com, self.lfoot, self.rfoot,
        # self.com

        if self.com_abs[2] < 0.3:
            print 'FALL:', self.state

            if self.state == 'right':
                s = '2 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                                                                          self.lfoot_pos_com[
                                                                              0], self.lfoot_pos_com[1],
                                                                          self.lfoot_pos_com[
                                                                              2], self.lfoot_vel_com[0],
                                                                          self.lfoot_vel_com[
                                                                              1], self.lfoot_vel_com[2],
                                                                          self.rfoot_pos_com[
                                                                              0], self.rfoot_pos_com[1],
                                                                          self.rfoot_pos_com[
                                                                              2], self.rfoot_vel_com[0],
                                                                          self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            elif self.state == 'left':
                s = '3 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                                                                          self.lfoot_pos_com[
                                                                              0], self.lfoot_pos_com[1],
                                                                          self.lfoot_pos_com[
                                                                              2], self.lfoot_vel_com[0],
                                                                          self.lfoot_vel_com[
                                                                              1], self.lfoot_vel_com[2],
                                                                          self.rfoot_pos_com[
                                                                              0], self.rfoot_pos_com[1],
                                                                          self.rfoot_pos_com[
                                                                              2], self.rfoot_vel_com[0],
                                                                          self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            elif self.state == 'fly':
                s = '4 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                                                                          self.lfoot_pos_com[
                                                                              0], self.lfoot_pos_com[1],
                                                                          self.lfoot_pos_com[
                                                                              2], self.lfoot_vel_com[0],
                                                                          self.lfoot_vel_com[
                                                                              1], self.lfoot_vel_com[2],
                                                                          self.rfoot_pos_com[
                                                                              0], self.rfoot_pos_com[1],
                                                                          self.rfoot_pos_com[
                                                                              2], self.rfoot_vel_com[0],
                                                                          self.rfoot_vel_com[1], self.rfoot_vel_com[2])
            else:

                s = '5 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                                                                          self.lfoot_pos_com[
                                                                              0], self.lfoot_pos_com[1],
                                                                          self.lfoot_pos_com[
                                                                              2], self.lfoot_vel_com[0],
                                                                          self.lfoot_vel_com[
                                                                              1], self.lfoot_vel_com[2],
                                                                          self.rfoot_pos_com[
                                                                              0], self.rfoot_pos_com[1],
                                                                          self.rfoot_pos_com[
                                                                              2], self.rfoot_vel_com[0],
                                                                          self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            self.fdyn.write(s)
            self.fdyn_mid.write(s)

            poppy.reset_simulation()
            print 'resetting'
        else:

            if self.robot.CollisionLFoot.colliding and not self.robot.CollisionRFoot.colliding:
                # Only LFoot on ground
                self.state = 'right'
                # print 'l vel', self.lfoot_vel_com
                # print 'lfoot', self.lfoot_pos_com
                s = '0 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                                                                          self.lfoot_pos_com[
                                                                              0], self.lfoot_pos_com[1],
                                                                          self.lfoot_pos_com[
                                                                              2], self.lfoot_vel_com[0],
                                                                          self.lfoot_vel_com[
                                                                              1], self.lfoot_vel_com[2],
                                                                          self.rfoot_pos_com[
                                                                              0], self.rfoot_pos_com[1],
                                                                          self.rfoot_pos_com[
                                                                              2], self.rfoot_vel_com[0],
                                                                          self.rfoot_vel_com[1], self.rfoot_vel_com[2])

                self.fdyn.write(s)

            elif not self.robot.CollisionLFoot.colliding and self.robot.CollisionRFoot.colliding:
                # only RFoot on ground
                self.state = 'left'
                # print 'r vel', self.rfoot_vel_com
                # s = '1 %f %f %f %f %f %f\n' % (self.rfoot_pos_com[0], self.rfoot_pos_com[1],
                # self.rfoot_pos_com[2], self.rfoot_vel_com[0],
                # self.rfoot_vel_com[1], self.rfoot_vel_com[2])
                s = '1 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                                                                          self.lfoot_pos_com[
                                                                              0], self.lfoot_pos_com[1],
                                                                          self.lfoot_pos_com[
                                                                              2], self.lfoot_vel_com[0],
                                                                          self.lfoot_vel_com[
                                                                              1], self.lfoot_vel_com[2],
                                                                          self.rfoot_pos_com[
                                                                              0], self.rfoot_pos_com[1],
                                                                          self.rfoot_pos_com[
                                                                              2], self.rfoot_vel_com[0],
                                                                          self.rfoot_vel_com[1], self.rfoot_vel_com[2])

                self.fdyn.write(s)

            elif self.robot.CollisionLFoot.colliding and self.robot.CollisionRFoot.colliding:
                # Both Feet on ground. Take the decision here.
                self.state = 'both'

            else:
                # gnin?
                print 'FLY'
                self.state = 'fly'
                poppy.reset_simulation()
                print "resetting"

        # print 'Foot on ground L R:', self.robot.CollisionLFoot.colliding,
        # self.robot.CollisionRFoot.colliding, self.state

    def run(self):

        while True:
            self.update()

            if self.state == 'right':
                time.sleep(0.05)
                # pass
            elif self.state == 'left':
                time.sleep(0.05)
                # pass
            elif self.state == 'both':
                # decide and move
                leg, x, y = self.get_goal()
                print 'GOAL', leg, x, y
                # print leg, x, y
                self.goal_x = x
                self.goal_y = y
                if leg == 'left':
                    self.goal_leg = 1
                elif leg == 'right':
                    self.goal_leg = 0

                s = ''
                if leg == 'left':
                    s += '0 '
                else:
                    s += '1 '
                s += '%f %f\n' % (x, y)
                self.fgoals.write(s)
                self.move(leg, x, y)
            else:
                print '?????'

    def get_goal(self):

        lift = ''
        x = 0
        y = 0
        # random left of right
        if random.randrange(2) == 0:
            # left = > y > 0
            lift = 'left'
            y = random.random() * 0.2 + 0.1
            x = random.random() * 0.4 - 0.2

        else:
            lift = 'right'
            y = random.random() * -0.2 - 0.1
            x = random.random() * 0.4 - 0.2

        return lift, x, y

    def move(self, leg, x, y):

        if leg == 'left':
            self.update()
            self.robot.lleg3d_up.add(
                0, 0, self.duration / 2.0)
            # self.robot.lleg3d_up.add(0, 0, 0.1)
            self.robot.lleg3d_up.start()
            self.robot.lleg3d_up.wait_to_start()
            self.robot.lleg3d_up.wait_to_stop()
            self.update()

            # s = '0 %f %f %f %f %f %f\n' % (self.lfoot_pos_com[0], self.lfoot_pos_com[1],
            # self.lfoot_pos_com[2], self.lfoot_vel_com[0],
            # self.lfoot_vel_com[1], self.lfoot_vel_com[2])

            s = '0 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                                                                      self.lfoot_pos_com[
                                                                          0], self.lfoot_pos_com[1],
                                                                      self.lfoot_pos_com[
                                                                          2], self.lfoot_vel_com[0],
                                                                      self.lfoot_vel_com[
                                                                          1], self.lfoot_vel_com[2],
                                                                      self.rfoot_pos_com[
                                                                          0], self.rfoot_pos_com[1],
                                                                      self.rfoot_pos_com[
                                                                          2], self.rfoot_vel_com[0],
                                                                      self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            self.fdyn_mid.write(s)

            self.robot.lleg3d_down.add(
                x, y, self.duration / 2.0)

            self.robot.lleg3d_down.start()
            self.robot.lleg3d_down.wait_to_start()
            self.robot.lleg3d_down.wait_to_stop()

        elif leg == 'right':

            self.update()
            self.robot.rleg3d_up.add(0, 0, self.duration / 2.0)

            # self.robot.rleg3d_up.add(0, 0, 0.1)
            self.robot.rleg3d_up.start()
            self.robot.rleg3d_up.wait_to_start()
            self.robot.rleg3d_up.wait_to_stop()

            self.update()
            # print 'r vel', self.rfoot_vel_com

            # s = '1 %f %f %f %f %f %f\n' % (self.rfoot_pos_com[0], self.rfoot_pos_com[1],
            # self.rfoot_pos_com[2], self.rfoot_vel_com[0],
            # self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            s = '1 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                                                                      self.lfoot_pos_com[
                                                                          0], self.lfoot_pos_com[1],
                                                                      self.lfoot_pos_com[
                                                                          2], self.lfoot_vel_com[0],
                                                                      self.lfoot_vel_com[
                                                                          1], self.lfoot_vel_com[2],
                                                                      self.rfoot_pos_com[
                                                                          0], self.rfoot_pos_com[1],
                                                                      self.rfoot_pos_com[
                                                                          2], self.rfoot_vel_com[0],
                                                                      self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            self.fdyn_mid.write(s)

            self.robot.rleg3d_down.add(
                x, y, self.duration / 2.0)
            # self.robot.rleg3d_down.add( - 0.04 ,- 0.05 , self.duration / 2.0)
            self.robot.rleg3d_down.start()
            self.robot.rleg3d_down.wait_to_start()
            self.robot.rleg3d_down.wait_to_stop()


# poppy = from_vrep(poppy_config, '127.0.0.1', 19997, 'poppy_sensors.ttt' )
# poppy = from_vrep(poppy_config, '127.0.0.1', 19997,  'poppy_testings2.ttt')
# poppy = from_vrep(poppy_config, '127.0.0.1', 19997,  'poppy_v1.1.ttt')
poppy = from_vrep(poppy_config, '127.0.0.1', 19997, 'poppy-standing-hightorque.ttt',
                  tracked_collisions=['CollisionRFoot', 'CollisionLFoot'])
# poppy = from_vrep(poppy_config, '127.0.0.1', 19997, 'poppy_standing2_fun.ttt')
# poppy = from_vrep(poppy_config, '127.0.0.1', 19997,  'poppy_standing.ttt')

# time.sleep(10)
client_id = poppy._controllers[0].io.client_id
print 'ID', client_id


# thread safe stuff

vrepio = poppy._controllers[0].io


obj_h = {}

while obj_h == {}:

    # objects = vrep.simxGetObjectGroupData(
        # client_id, vrep.sim_object_shape_type, 0,
        # vrep.simx_opmode_oneshot_wait)

    objects = vrepio.call_remote_api('simxGetObjectGroupData',
                                     vrep.sim_object_shape_type, 0)

    obj_h = dict(zip(objects[3], objects[0]))
    print 'OBJ:', obj_h


joint_h = {}
while joint_h == {}:

    # joints = vrep.simxGetObjectGroupData(
    # client_id, vrep.sim_object_joint_type, 0, vrep.simx_opmode_oneshot_wait)

    joints = vrepio.call_remote_api('simxGetObjectGroupData',
                                    vrep.sim_object_joint_type, 0)

    joint_h = dict(zip(joints[3], joints[0]))
    print 'JOINT:', joint_h


dummies_h = {}
while dummies_h == {}:

    # dummies = vrep.simxGetObjectGroupData(
    # client_id, vrep.sim_object_dummy_type, 0, vrep.simx_opmode_oneshot_wait)

    dummies = vrepio.call_remote_api('simxGetObjectGroupData',
                                     vrep.sim_object_dummy_type, 0)

    dummies_h = dict(zip(dummies[3], dummies[0]))
    print 'DUMMY:', dummies_h


# collisions_h = {}
# while collisions_h == {}:

#     collisions = vrep.simxGetObjectGroupData(
#         client_id, vrep.sim_appobj_collision_type, 0, vrep.simx_opmode_oneshot_wait)
#     collisions_h = dict(zip(collisions[4], collisions[1]))
#     print 'COLLISION:', collisions_h

# e, cl = vrep.simxGetCollisionHandle(
#     client_id, 'CollisionLFoot', vrep.simx_opmode_oneshot_wait)

# print e, cl

# e, cr = vrep.simxGetCollisionHandle(
#     client_id, 'CollisionRFoot', vrep.simx_opmode_oneshot_wait)

# print e, cr

# vrep.simxReadCollision(client_id, cl, streaming=True)
# vrep.simxReadCollision(client_id, cr, streaming=True)


# e = vrep.simxLoadScene(client_id, 'test_poppy.ttt', 0, vrep.simx_opmode_oneshot_wait)
# print e
# e = vrep.simxStartSimulation(client_id, vrep.simx_opmode_oneshot)
# print e

vrepio.start_simulation()

poppy.start_sync()

# primitive manager combination function


poppy._primitive_manager._filter = np.sum

# time.sleep(0.1)


# obj_h = vrep.simxGetObjects(client_id, vrep.sim_object_shape_type, vrep.simx_opmode_oneshot_wait)

# objects = vrep.simxGetObjectGroupData(
#     client_id, vrep.sim_object_shape_type, 0, vrep.simx_opmode_oneshot_wait)


imu = vrep_tools.VrepIMU(client_id, obj_h['head_visual'])
imu.start()


poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_start()

# time.sleep(1)

poppy.stand_position.wait_to_stop()
# poppy.stand_position.stop()
# print poppy._primitive_manager._prim
# time.sleep(1)
# poppy.attach_primitive(kinematics_dev.StabilizeTrunkYHipAnkle(poppy, imu, kp=1.0 / 100.0, kd = 0.0), 'stab')
# poppy.stab.start()
# poppy.stab.wait_to_start()


# poppy.attach_primitive(kinematics_dev.StabilizeTrunkY(poppy, imu, kp=1.0 / 300.0), 'stab')
# poppy.stab.start()
# poppy.stab.wait_to_start()

# time.sleep(30)

# stop
# poppy.attach_primitive(kinematics_dev.SymmetricLegs(poppy), 'sym')


# poppy.r_hip_x.goal_position =- 15
# poppy.l_hip_x.goal_position = 15


# poppy.attach_primitive(min_jerk.MJLegsUp3D(poppy, 'left'), 'lleg3d_up')
# poppy.attach_primitive(min_jerk.MJLegsUp3D(poppy, 'right'), 'rleg3d_up')

# poppy.attach_primitive(min_jerk.MJLegsDown3D(poppy, 'left'), 'lleg3d_down')
# poppy.attach_primitive(min_jerk.MJLegsDown3D(poppy, 'right'), 'rleg3d_down')


# poppy.lleg3d_up.add( - 0.1, 0.1, 0.15 / 2.0)
# poppy.lleg3d_up.add(0, 0, 0.1)
# poppy.lleg3d_up.start()
# poppy.lleg3d_up.wait_to_start()
# poppy.lleg3d_up.wait_to_stop()

# poppy.lleg3d_down.add( - 0.1, 0.1, 0.15 / 2.0)
# poppy.lleg3d_down.add( - 0.2 ,0.05 , self.duration / 2.0)
# poppy.lleg3d_down.start()
# poppy.lleg3d_down.wait_to_start()
# poppy.lleg3d_down.wait_to_stop()


# stop
print 'GO'
dyn = LearnStab(poppy, client_id, obj_h, dummies_h)
# dyn.update()
# time.sleep(0.1)
# dyn.update()
# time.sleep(1)
# dyn.update()


dyn.run()

stop
