#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_vrep.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Thursday, July  3 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import time
import sys
sys.path.append('/home/steve/divers/V-REP_PRO_EDU_V3_1_2_64_Linux/programming/remoteApiBindings/python/python/')

from pypot.vrep import from_vrep

import pypot.vrep

import poppytools.primitive.basic as basic

from poppytools.configuration.config import poppy_config

import vrep

pypot.vrep.close_all_connections()

vrep_error = {'ok': vrep.simx_return_ok,
              'novalue': vrep.simx_return_novalue_flag,
              'timeout': vrep.simx_return_timeout_flag,
              'opmode': vrep.simx_return_illegal_opmode_flag,
              'remote': vrep.simx_return_remote_error_flag,
              'progress': vrep.simx_return_split_progress_flag,
              'local': vrep.simx_return_local_error_flag,
              'init': vrep.simx_return_initialize_error_flag}


def check_error(err):
    for e, c in vrep_error.iteritems():
        if err == c:
            print e


poppy = from_vrep(poppy_config, '127.0.0.1', 19997, 'poppy_v1.ttt' )
# poppy = from_vrep(poppy_config, '127.0.0.1', 19997, 'poppy_standing.ttt' )
# poppy = from_vrep(poppy_config, '127.0.0.1', 19997 )

client_id = poppy._controllers[0].io.client_id


objects = vrep.simxGetObjectGroupData(client_id, vrep.sim_object_shape_type, 0, vrep.simx_opmode_oneshot_wait)
obj_h = dict(zip(objects[4], objects[1]))
print 'OBJ:', obj_h


objects = vrep.simxGetObjectGroupData(client_id, vrep.sim_object_joint_type, 0, vrep.simx_opmode_oneshot_wait)
obj_h = dict(zip(objects[4], objects[1]))
print 'JOINT:', obj_h


objects = vrep.simxGetObjectGroupData(client_id, vrep.sim_object_dummy_type, 0, vrep.simx_opmode_oneshot_wait)
obj_h = dict(zip(objects[4], objects[1]))
print 'DUMMY:', obj_h



# e = vrep.simxLoadScene(client_id, 'test_poppy.ttt', 0, vrep.simx_opmode_oneshot_wait)
# print e
e = vrep.simxStartSimulation(client_id, vrep.simx_opmode_oneshot)
print e

poppy.start_sync()


time.sleep(5)


# obj_h = vrep.simxGetObjects(client_id, vrep.sim_object_shape_type, vrep.simx_opmode_oneshot_wait)

objects = vrep.simxGetObjectGroupData(client_id, vrep.sim_object_shape_type, 0, vrep.simx_opmode_oneshot_wait)


obj_h = dict(zip(objects[4], objects[1]))

print 'OBJ:', obj_h

poppy.attach_primitive(basic.StandPosition(poppy), 'stand')
poppy.stand.start()

time.sleep(2)
poppy.reset_simulation()

for i in range(1000):


    # print head_pos

    # for m in poppy.motors:
    #     print m.name, m.present_position
    #     m.goal_position += 0.1
    #     # m.goal_position -= 4.

    # for m in poppy.arms:
    #     print m.name, m.present_position
    #     m.goal_position += 0.1
    #     # m.goal_position -= 4.

    poppy.r_elbow_y.goal_position += 0.1
    poppy.l_elbow_y.goal_position += 0.1


    time.sleep(0.02)

# e = vrep.simxStopSimulation(client_id, vrep.simx_opmode_oneshot)
# print e

time.sleep(1)



# poppy.reset_simulation()
