#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_vrep.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Thursday, July  3 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import random
import numpy as np
import math
import time
import sys
sys.path.append('/home/steve/divers/V-REP_PRO_EDU_V3_1_2_64_Linux/programming/remoteApiBindings/python/python/')

sys.path.append('/home/steve/Project/Repo/poppy-software/example/') #fixme

from pypot.vrep import from_vrep
import pypot.vrep
import vrep

from poppytools.configuration.config import poppy_config

# import poppytools.utils.vrep_tools as vrep_tools


import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics
import poppytools.utils.kinematics_dev as kinematics_dev
import poppytools.utils.min_jerk as min_jerk



import wx
import robot_gui



# pypot.vrep.close_all_connections()

vrep_error = {'ok': vrep.simx_return_ok,
              'novalue': vrep.simx_return_novalue_flag,
              'timeout': vrep.simx_return_timeout_flag,
              'opmode': vrep.simx_return_illegal_opmode_flag,
              'remote': vrep.simx_return_remote_error_flag,
              'progress': vrep.simx_return_split_progress_flag,
              'local': vrep.simx_return_local_error_flag,
              'init': vrep.simx_return_initialize_error_flag}


def check_error(err):
    for e, c in vrep_error.iteritems():
        if err == c:
            return e


class get_vrep_time():
    def __init__(self, client_id):
        self.client_id = client_id

        #todo check error
    def get_time(self):

        res, tt = vrep.simxGetFloatSignal(self.client_id, 'CurrentTime', vrep.simx_opmode_buffer)
        return tt



# poppy = from_vrep(poppy_config, '127.0.0.1', 19997, 'poppy_sensors.ttt' )
# poppy = from_vrep(poppy_config, '127.0.0.1', 19997,  'poppy_testings2.ttt')
# poppy = from_vrep(poppy_config, '127.0.0.1', 19997,  'poppy_v1.1.ttt')
poppy = from_vrep(poppy_config, '127.0.0.1', 19997, 'poppy_standing2.ttt')

# poppy = from_vrep(poppy_config, '127.0.0.1', 19997,  'poppy_standing.ttt')

client_id = poppy._controllers[0].io.client_id

vreptime = get_vrep_time(client_id)

objects = vrep.simxGetObjectGroupData(client_id, vrep.sim_object_shape_type, 0, vrep.simx_opmode_oneshot_wait)
obj_h = dict(zip(objects[4], objects[1]))
print 'OBJ:', obj_h


objects = vrep.simxGetObjectGroupData(client_id, vrep.sim_object_joint_type, 0, vrep.simx_opmode_oneshot_wait)
obj_h = dict(zip(objects[4], objects[1]))
print 'JOINT:', obj_h


objects = vrep.simxGetObjectGroupData(client_id, vrep.sim_object_dummy_type, 0, vrep.simx_opmode_oneshot_wait)
obj_h = dict(zip(objects[4], objects[1]))
print 'DUMMY:', obj_h



# e = vrep.simxLoadScene(client_id, 'test_poppy.ttt', 0, vrep.simx_opmode_oneshot_wait)
# print e
e = vrep.simxStartSimulation(client_id, vrep.simx_opmode_oneshot)
print e

poppy.start_sync()

#primitive manager combination function

poppy._primitive_manager._filter = np.sum

time.sleep(5)


# obj_h = vrep.simxGetObjects(client_id, vrep.sim_object_shape_type, vrep.simx_opmode_oneshot_wait)

objects = vrep.simxGetObjectGroupData(client_id, vrep.sim_object_shape_type, 0, vrep.simx_opmode_oneshot_wait)


obj_h = dict(zip(objects[4], objects[1]))

print 'OBJ:', obj_h

# imu = vrep_tools.VrepIMU(client_id, obj_h['head_visual'])
# imu.start()


res, tt = vrep.simxGetFloatSignal(client_id, 'CurrentTime', vrep.simx_opmode_streaming)
print tt

poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()

# time.sleep(3)
# poppy.r_hip_x.goal_position =- 15
# poppy.l_hip_x.goal_position = 15


# poppy.r_ankle_x.goal_position = 5
# poppy.l_ankle_x.goal_position =- 5

# time.sleep(5)

# stop
# poppy.reset_simulation()





# poppy.attach_primitive(min_jerk.MoveGprojMJ(poppy,0.0, 2.0), 'gprojmj')
# poppy.gprojmj.start()
# poppy.gprojmj.wait_to_stop()


up_duration = 0.1
down_duration = 0.1

up = 0.05
# up = 0.00


# duration = 2.0
# up = 0.05


mj1 = min_jerk.MJTraj(0, up, up_duration)
mj2 = min_jerk.MJTraj(up, 0, down_duration)



poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "left", mj1, 1), 'mjleftup')
poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "left", mj2, 1), 'mjleftdown')

poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "right", mj1, 1), 'mjrightup')
poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "right", mj2, 1), 'mjrightdown')



# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "left", mj1, 1, get_time = vreptime.get_time), 'mjleftup')
# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "left", mj2, 1, get_time = vreptime.get_time), 'mjleftdown')

# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "right", mj1, 1, get_time = vreptime.get_time), 'mjrightup')
# poppy.attach_primitive(min_jerk.MJLegs1D(poppy, "right", mj2, 1, get_time = vreptime.get_time), 'mjrightdown')


X_COM = 0.03
COM_DURATION = 0.15
HIP_OFFSET = 3

STATE = 'RIGHT'

# for i in range(8):

#     if STATE == 'RIGHT':

#         poppy.gprojmj.add(  X_COM, COM_DURATION)
#         poppy.gprojmj.start()
#         poppy.gprojmj.wait_to_stop()

#         poppy.mjrightup.start()
#         poppy.r_hip_x.goal_position = poppy.r_hip_x.present_position - HIP_OFFSET
#         poppy.mjrightup.wait_to_stop()



#         poppy.mjrightdown.start()
#         poppy.mjrightdown.wait_to_stop()
#         STATE = 'LEFT'

#     elif STATE == 'LEFT':

#         poppy.gprojmj.add( - X_COM, COM_DURATION)
#         poppy.gprojmj.start()
#         poppy.gprojmj.wait_to_stop()

#         poppy.mjleftup.start()
#         poppy.l_hip_x.goal_position = poppy.l_hip_x.present_position + HIP_OFFSET
#         poppy.mjleftup.wait_to_stop()

#         poppy.mjleftdown.start()
#         poppy.mjleftdown.wait_to_stop()
#         STATE = 'RIGHT'






prev = tt


# while True:
#     Time=vrep.simxGetLastCmdTime(client_id)
#     res, tt = vrep.simxGetFloatSignal(client_id, 'CurrentTime', vrep.simx_opmode_buffer)

#     print Time, tt, tt - prev
#     prev = tt


# stop



time.sleep(3)
poppy.r_hip_x.goal_position =- 15
poppy.l_hip_x.goal_position = 15


poppy.r_ankle_x.goal_position = 5
poppy.l_ankle_x.goal_position =- 5

time.sleep(5)


for i in range(20):
    poppy.r_hip_x.goal_position =- 15
    poppy.l_hip_x.goal_position = 15


    poppy.r_ankle_x.goal_position = 5
    poppy.l_ankle_x.goal_position =- 5

    Time=vrep.simxGetLastCmdTime(client_id)
    res, tt = vrep.simxGetFloatSignal(client_id, 'CurrentTime', vrep.simx_opmode_buffer)

    print
    print Time, tt, tt - prev

    prev = tt
    if STATE == 'RIGHT':
        print 'RIGHT'

        poppy.mjrightup.start()
        poppy.mjrightup.wait_to_start()
        # poppy.r_hip_x.goal_position = poppy.r_hip_x.present_position - HIP_OFFSET
        poppy.mjrightup.wait_to_stop()
        # time.sleep(1)
        # poppy.mjrightup.stop()
        # print '  RIGHT UP STOP', vrep.simxGetFloatSignal(client_id, 'CurrentTime', vrep.simx_opmode_buffer)[1]


        poppy.mjrightdown.start()
        poppy.mjrightdown.wait_to_start()
        poppy.mjrightdown.wait_to_stop()
        # time.sleep(1)

        # poppy.mjrightdown.stop()
        # print '  RIGHT DOWN STOP', vrep.simxGetFloatSignal(client_id, 'CurrentTime', vrep.simx_opmode_buffer)[1]

        STATE = 'LEFT'

    elif STATE == 'LEFT':
        print 'LEFT'

        poppy.mjleftup.start()
        poppy.mjleftup.wait_to_start()

        # poppy.l_hip_x.goal_position = poppy.l_hip_x.present_position + HIP_OFFSET
        poppy.mjleftup.wait_to_stop()
        # time.sleep(1)

        # poppy.mjleftup.stop()
        # print '  LEFT UP STOP', vrep.simxGetFloatSignal(client_id, 'CurrentTime', vrep.simx_opmode_buffer)[1]


        poppy.mjleftdown.start()
        poppy.mjleftdown.wait_to_start()

        poppy.mjleftdown.wait_to_stop()
        # time.sleep(1)

        # poppy.mjleftdown.stop()
        # print '  LEFT DOWN STOP', vrep.simxGetFloatSignal(client_id, 'CurrentTime', vrep.simx_opmode_buffer)[1]
        STATE = 'RIGHT'

    # time.sleep(0.01)

time.sleep(1)

poppy.r_hip_x.goal_position =- 15
poppy.l_hip_x.goal_position = 15


poppy.r_ankle_x.goal_position = 5
poppy.l_ankle_x.goal_position =- 5

time.sleep(5)

poppy.reset_simulation()
poppy.stand_position.start()
poppy.stand_position.wait_to_stop()

time.sleep(1)
