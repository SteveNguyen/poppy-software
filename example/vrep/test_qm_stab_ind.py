#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'test_vrep.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@college-de-france.fr
#  Created	: Thursday, July  3 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import random
import numpy as np
import math
import time as systime
import pypot.utils.pypot_time as time
import sys
# sys.path.append('/home/steve/divers/V-REP_PRO_EDU_V3_1_2_64_Linux/programming/remoteApiBindings/python/python/')

# sys.path.append(
# '/home/steve/divers/V-REP/programming/remoteApiBindings/python/python/')


sys.path.append('/home/steve/Project/Repo/poppy-software/example/')  # fixme

sys.path.append(
    '/home/steve/Project/Repo/quasi-distance/qmlib/python')  # fixme

import onlineqm_poppy_simu_ind as poppy_qm


from pypot.vrep import from_vrep
import pypot.vrep
# import vrep
import pypot.vrep.remoteApiBindings.vrep as vrep

from poppytools.configuration.config import poppy_config

# import poppytools.utils.vrep_tools as vrep_tools


# import poppytools.primitive.basic_dev as basic
import poppytools.utils.kinematics as kinematics_dev
# import poppytools.utils.kinematics_dev as kinematics_dev
import poppytools.utils.min_jerk as min_jerk

import pypot


def online_from_file_ind(filename, qmx, qmy):
    fd = open(filename, 'r')
    data = []
    for l in fd:
        if not l[0] == '#':
            l = l.replace('\n', '').split(' ')
            # print l

            d = np.array([float(d) for d in l if d != ''])
            data.append(d)

            # xy = XY.Discretize((d[0], d[1], d[2], d[3]))
            # u = U.Discretize((d[4], d[5]))
            # next_xy = XY.Discretize((d[6], d[7], d[8], d[9]))

            xpos = Xpos.Discretize(d[0])
            ypos = Ypos.Discretize(d[1])
            xvel = Xvel.Discretize(d[2])
            yvel = Yvel.Discretize(d[3])

            ux = Ux.Discretize(d[4])
            uy = Uy.Discretize(d[5])

            n_xpos = Xpos.Discretize(d[6])
            n_ypos = Ypos.Discretize(d[7])
            n_xvel = Xvel.Discretize(d[8])
            n_yvel = Yvel.Discretize(d[9])

            # print d[0], xpos, d[1], ypos, d[2], xvel, d[3], yvel, d[6],
            # n_xpos, d[7], n_ypos, d[8], n_xvel, d[9], n_yvel, d[4], ux, d[5],
            # uy

            # qm.OnlineUpdate(
            #     (xpos, ypos, xvel, yvel), (n_xpos, n_ypos, n_xvel, n_yvel), (ux, uy), cost((), ()))

            qmx.OnlineUpdate(
                (xpos, xvel), (n_xpos, n_xvel), (ux, ), cost((), ()))
            qmy.OnlineUpdate(
                (ypos, yvel), (n_ypos, n_yvel), (uy, ), cost((), ()))


class Stab():

    def __init__(self,  robot, client_id, obj_h, dummies_h, quasix, quasiy):

        self.quasix = quasix
        self.quasiy = quasiy
        self.fall = False

        xpos = poppy_qm.Xpos.Discretize(0.0)
        ypos = poppy_qm.Ypos.Discretize(0.05)
        xvel = poppy_qm.Xvel.Discretize(0.0)
        yvel = poppy_qm.Yvel.Discretize(0.0)

        goalx = (xpos, xvel)
        goaly = (ypos, yvel)

        self.goal_vertx = quasix.Vertices[poppy_qm.ProbUtils.totuple(goalx)]
        self.goal_verty = quasiy.Vertices[poppy_qm.ProbUtils.totuple(goaly)]

        self.nb_reset = 0

        self.fgoals = open('stab_goals_qm.dat', 'w+')
        self.fdyn = open('stab_dyn_qm.dat', 'w+')

        self.fdyn_mid = open('stab_dyn_mid_qm.dat', 'w+')

        self.transitions_log = open('onlineqm_trans.dat', 'a+')
        self.transitions_logd = open('onlineqm_transd.dat', 'a+')

        self.robot = robot
        self.state = 'right'
        self.goal_leg = 0

        self.duration = 0.25
        self.client_id = client_id
        self.up = 0.1

        self.vrepio = self.robot._controllers[0].io

        # self.lfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
        #     'foot_left_visual'], obj_h['base_link_visual'], streaming=True)

        # self.rfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
        #     'foot_right_visual'], obj_h['base_link_visual'], streaming=True)

        # self.lfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
        #     'foot_left_visual'], - 1, streaming=True)

        # self.rfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
        #     'foot_right_visual'], - 1, streaming=True)

        self.lfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
            'foot_left_visual'], dummies_h['CoM'], streaming=True)

        self.rfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
            'foot_right_visual'], dummies_h['CoM'], streaming=True)

        # self.com = self.vrepio.call_remote_api('simxGetObjectPosition', dummies_h['CoM'], obj_h[
        #     'base_link_visual'], streaming=True)

        self.com_abs = self.vrepio.call_remote_api(
            'simxGetObjectPosition', dummies_h['CoM'], - 1, streaming=True)

        self.com_vel = self.vrepio.call_remote_api(
            'simxGetObjectVelocity', dummies_h['CoM'], streaming=True)

        # self.lfoot = np.array(
        #     [- self.lfoot[0], - self.lfoot[1], self.lfoot[2]])
        # self.rfoot = np.array(
        #     [- self.rfoot[0], - self.rfoot[1], self.rfoot[2]])
        # self.com = np.array([- self.com[0], - self.com[1], self.com[2]])

        self.lfoot = np.array(
            [self.lfoot[0], self.lfoot[1], self.lfoot[2]])
        self.rfoot = np.array(
            [self.rfoot[0], self.rfoot[1], self.rfoot[2]])
        self.com = np.array(
            [self.com_abs[0], self.com_abs[1], self.com_abs[2]])

        # self.lfoot_pos_com = np.array(self.lfoot) - np.array(self.com)
        # self.rfoot_pos_com = np.array(self.rfoot) - np.array(self.com)

        self.lfoot_pos_com = np.array(self.lfoot)
        self.rfoot_pos_com = np.array(self.rfoot)

        self.lfoot_vel_com = np.array([0, 0, 0])
        self.rfoot_vel_com = np.array([0, 0, 0])

        self.robot.attach_primitive(
            min_jerk.MJLegsUp3D(self.robot, 'left', self.up), 'lleg3d_up')
        self.robot.attach_primitive(
            min_jerk.MJLegsUp3D(self.robot, 'right', self.up), 'rleg3d_up')

        self.robot.attach_primitive(
            min_jerk.MJLegsDown3D(self.robot, 'left', self.up), 'lleg3d_down')
        self.robot.attach_primitive(
            min_jerk.MJLegsDown3D(self.robot, 'right', self.up), 'rleg3d_down')

        self.t0 = time.time()
        # time.sleep(0.1)

        self.update()
        self.current_state = (self.rfoot_pos_com[0], -self.rfoot_pos_com[
            1], self.rfoot_vel_com[0], -self.rfoot_vel_com[1])
        self.current_cmd = None

    def update(self):

        # self.lfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
        #     'foot_left_visual'], obj_h['base_link_visual'], streaming=True)

        # self.rfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
        #     'foot_right_visual'], obj_h['base_link_visual'], streaming=True)

        self.lfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
            'foot_left_visual'], dummies_h['CoM'], streaming=True)

        self.rfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
            'foot_right_visual'], dummies_h['CoM'], streaming=True)

        # self.lfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
        #     'foot_left_visual'], - 1, streaming=True)

        # self.rfoot = self.vrepio.call_remote_api('simxGetObjectPosition', obj_h[
        #     'foot_right_visual'], - 1, streaming=True)

        # self.com = self.vrepio.call_remote_api('simxGetObjectPosition', dummies_h['CoM'], obj_h[
        #     'base_link_visual'], streaming=True)

        self.com_abs = self.vrepio.call_remote_api(
            'simxGetObjectPosition', dummies_h['CoM'], - 1, streaming=True)

        self.com_vel = self.vrepio.call_remote_api(
            'simxGetObjectVelocity', dummies_h['CoM'], streaming=True)

        # print self.com_abs

        # self.lfoot = np.array(
        #     [- self.lfoot[0], - self.lfoot[1], self.lfoot[2]])
        # self.rfoot = np.array(
        #     [- self.rfoot[0], - self.rfoot[1], self.rfoot[2]])
        # self.com = np.array([- self.com[0], - self.com[1], self.com[2]])

        self.lfoot = np.array(
            [self.lfoot[0], self.lfoot[1], self.lfoot[2]])
        self.rfoot = np.array(
            [self.rfoot[0], self.rfoot[1], self.rfoot[2]])
        self.com = np.array(
            [self.com_abs[0], self.com_abs[1], self.com_abs[2]])

        # lfoot_pos_com = np.array(self.lfoot) - np.array(self.com)
        # rfoot_pos_com = np.array(self.rfoot) - np.array(self.com)

        lfoot_pos_com = np.array(self.lfoot)
        rfoot_pos_com = np.array(self.rfoot)

        dt = time.time() - self.t0
        self.t0 = time.time()

        if dt <= 0.0:
            self.lfoot_vel_com = np.array([0, 0, 0])
            self.rfoot_vel_com = np.array([0, 0, 0])
        else:

            self.lfoot_vel_com = (
                np.array(lfoot_pos_com) - np.array(self.lfoot_pos_com)) / dt
            self.rfoot_vel_com = (
                np.array(rfoot_pos_com) - np.array(self.rfoot_pos_com)) / dt

        self.lfoot_pos_com = lfoot_pos_com
        self.rfoot_pos_com = rfoot_pos_com

        # , self.lfoot, self.rfoot,

        # self.com

        if self.com_abs[2] < 0.3:
            print 'FALL:', self.state
            self.fall = True
            # if self.state == 'right':
            #     s = '2 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
            #                                                               self.lfoot_pos_com[
            #                                                                   0], self.lfoot_pos_com[1],
            #                                                               self.lfoot_pos_com[
            #                                                                   2], self.lfoot_vel_com[0],
            #                                                               self.lfoot_vel_com[
            #                                                                   1], self.lfoot_vel_com[2],
            #                                                               self.rfoot_pos_com[
            #                                                                   0], self.rfoot_pos_com[1],
            #                                                               self.rfoot_pos_com[
            #                                                                   2], self.rfoot_vel_com[0],
            # self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            # elif self.state == 'left':
            #     s = '3 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
            #                                                               self.lfoot_pos_com[
            #                                                                   0], self.lfoot_pos_com[1],
            #                                                               self.lfoot_pos_com[
            #                                                                   2], self.lfoot_vel_com[0],
            #                                                               self.lfoot_vel_com[
            #                                                                   1], self.lfoot_vel_com[2],
            #                                                               self.rfoot_pos_com[
            #                                                                   0], self.rfoot_pos_com[1],
            #                                                               self.rfoot_pos_com[
            #                                                                   2], self.rfoot_vel_com[0],
            # self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            # elif self.state == 'fly':
            #     s = '4 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
            #                                                               self.lfoot_pos_com[
            #                                                                   0], self.lfoot_pos_com[1],
            #                                                               self.lfoot_pos_com[
            #                                                                   2], self.lfoot_vel_com[0],
            #                                                               self.lfoot_vel_com[
            #                                                                   1], self.lfoot_vel_com[2],
            #                                                               self.rfoot_pos_com[
            #                                                                   0], self.rfoot_pos_com[1],
            #                                                               self.rfoot_pos_com[
            #                                                                   2], self.rfoot_vel_com[0],
            #                                                               self.rfoot_vel_com[1], self.rfoot_vel_com[2])
            # else:

            #     s = '5 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
            #                                                               self.lfoot_pos_com[
            #                                                                   0], self.lfoot_pos_com[1],
            #                                                               self.lfoot_pos_com[
            #                                                                   2], self.lfoot_vel_com[0],
            #                                                               self.lfoot_vel_com[
            #                                                                   1], self.lfoot_vel_com[2],
            #                                                               self.rfoot_pos_com[
            #                                                                   0], self.rfoot_pos_com[1],
            #                                                               self.rfoot_pos_com[
            #                                                                   2], self.rfoot_vel_com[0],
            # self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            # self.fdyn.write(s)
            # self.fdyn_mid.write(s)

            poppy.reset_simulation()
            print 'resetting'
            self.nb_reset += 1

            if self.nb_reset == 5:

                print self.quasix.Compute(self.goal_vertx)
                print
                print self.quasiy.Compute(self.goal_verty)
                print self.quasix.ComputePolicy()
                print
                print self.quasiy.ComputePolicy()
                self.nb_reset = 0

        else:

            if self.robot.CollisionLFoot.colliding and not self.robot.CollisionRFoot.colliding:
                # Only LFoot on ground
                self.state = 'right'
                # print 'l vel', self.lfoot_vel_com
                # print 'lfoot', self.lfoot_pos_com
                # s = '0 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                #                                                           self.lfoot_pos_com[
                #                                                               0], self.lfoot_pos_com[1],
                #                                                           self.lfoot_pos_com[
                #                                                               2], self.lfoot_vel_com[0],
                #                                                           self.lfoot_vel_com[
                #                                                               1], self.lfoot_vel_com[2],
                #                                                           self.rfoot_pos_com[
                #                                                               0], self.rfoot_pos_com[1],
                #                                                           self.rfoot_pos_com[
                #                                                               2], self.rfoot_vel_com[0],
                # self.rfoot_vel_com[1], self.rfoot_vel_com[2])

                # self.fdyn.write(s)

            elif not self.robot.CollisionLFoot.colliding and self.robot.CollisionRFoot.colliding:
                # only RFoot on ground
                self.state = 'left'
                # print 'r vel', self.rfoot_vel_com
                # s = '1 %f %f %f %f %f %f\n' % (self.rfoot_pos_com[0], self.rfoot_pos_com[1],
                # self.rfoot_pos_com[2], self.rfoot_vel_com[0],
                # self.rfoot_vel_com[1], self.rfoot_vel_com[2])
                # s = '1 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                #                                                           self.lfoot_pos_com[
                #                                                               0], self.lfoot_pos_com[1],
                #                                                           self.lfoot_pos_com[
                #                                                               2], self.lfoot_vel_com[0],
                #                                                           self.lfoot_vel_com[
                #                                                               1], self.lfoot_vel_com[2],
                #                                                           self.rfoot_pos_com[
                #                                                               0], self.rfoot_pos_com[1],
                #                                                           self.rfoot_pos_com[
                #                                                               2], self.rfoot_vel_com[0],
                # self.rfoot_vel_com[1], self.rfoot_vel_com[2])

                # self.fdyn.write(s)

            elif self.robot.CollisionLFoot.colliding and self.robot.CollisionRFoot.colliding:
                # Both Feet on ground. Take the decision here.
                self.state = 'both'

            else:
                # gnin?
                # print 'FLY'
                self.state = 'fly'

                # poppy.reset_simulation()
                # print "resetting"

        # print 'Foot on ground L R:', self.robot.CollisionLFoot.colliding,
        # self.robot.CollisionRFoot.colliding, self.state

    def run(self):

        while True:
            self.update()

            if self.state == 'right':
                # time.sleep(0.05)
                # print 'FEET', self.lfoot_pos_com, self.rfoot_pos_com
                pass

            elif self.state == 'left':
                # time.sleep(0.05)
                # print 'FEET', self.lfoot_pos_com, self.rfoot_pos_com
                pass
            elif self.state == 'both':
                print 'FEET', self.lfoot_pos_com, self.rfoot_pos_com

                if self.goal_leg == 1:
                    # previous was left
                    leg = 'right'
                    self.goal_leg = 0
                    self.goal_x = 0
                    self.goal_y = 0
                    print leg
                    self.move_up(leg)

                    state = (self.lfoot_pos_com[0], self.lfoot_pos_com[
                        1], self.lfoot_vel_com[0], self.lfoot_vel_com[1])

                    x, y = self.get_goal(state)
                    # get the goal
                    self.current_state = state
                    self.current_cmd = (x, y)

                    self.move_down(leg, x, y)

                elif self.goal_leg == 0:
                    # previous was right
                    leg = 'left'
                    self.goal_leg = 1
                    self.goal_x = 0
                    self.goal_y = 0
                    print leg
                    self.move_up(leg)

                    # get the goal
                    state = (self.rfoot_pos_com[0], -self.rfoot_pos_com[
                        1], self.rfoot_vel_com[0], -self.rfoot_vel_com[1])

                    x, y = self.get_goal(state)

                    self.current_state = state
                    self.current_cmd = (x, y)

                    self.move_down(leg, x, - y)

            # decide and move
            #     leg, x, y = self.get_goal()
            #     print 'GOAL', leg, x, y
            # print leg, x, y
            #     self.goal_x = x
            #     self.goal_y = y
            #     if leg == 'left':
            #         self.goal_leg = 1
            #     elif leg == 'right':
            #         self.goal_leg = 0

            #     s = ''
            #     if leg == 'left':
            #         s += '0 '
            #     else:
            #         s += '1 '
            #     s += '%f %f\n' % (x, y)
            #     self.fgoals.write(s)
            #     self.move(leg, x, y)
            # else:
            #     print '?????'

    def get_goal(self, state):

        # lift = ''
        # x = 0
        # y = 0
        # random left of right
        # if random.randrange(2) == 0:
        # left = > y > 0
        #     lift = 'left'
        #     y = random.random() * 0.2 + 0.1
        #     x = random.random() * 0.4 - 0.2

        # else:
        #     lift = 'right'
        #     y = random.random() * -0.2 - 0.1
        #     x = random.random() * 0.4 - 0.2

        # return lift, x, y

        xpos = poppy_qm.Xpos.Discretize(state[0])
        ypos = poppy_qm.Ypos.Discretize(state[1])
        xvel = poppy_qm.Xvel.Discretize(state[2])
        yvel = poppy_qm.Yvel.Discretize(state[3])

        # update
        if self.current_cmd is not None and not self.fall:
            oldxpos = poppy_qm.Xpos.Discretize(self.current_state[0])
            oldypos = poppy_qm.Ypos.Discretize(self.current_state[1])
            oldxvel = poppy_qm.Xvel.Discretize(self.current_state[2])
            oldyvel = poppy_qm.Yvel.Discretize(self.current_state[3])

            ux = poppy_qm.Ux.Discretize(self.current_cmd[0])
            uy = poppy_qm.Uy.Discretize(self.current_cmd[1])

            print 'UPDATE'
            self.quasix.OnlineUpdate(
                (oldxpos, oldxvel), (xpos, xvel), (self.current_cmd[0], ), 1.0)
            self.quasiy.OnlineUpdate(
                (oldypos, oldyvel), (ypos, yvel), (self.current_cmd[1], ), 1.0)

            s = '%d %d %d %d %d %d %d %d %d %d\n' % (
                oldxpos, oldypos, oldxvel, oldyvel, ux, uy, xpos, ypos, xvel, yvel)

            self.transitions_logd.write(s)
            self.transitions_logd.flush()

            s = '%f %f %f %f %f %f %f %f %f %f\n' % (self.current_state[0], self.current_state[1], self.current_state[
                2], self.current_state[3], self.current_cmd[0],
                self.current_cmd[1], state[0], state[1], state[2], state[3])

            self.transitions_log.write(s)
            self.transitions_log.flush()

        if self.fall:
            self.fall = False

        statex_d = (xpos, xvel)
        statey_d = (ypos, yvel)

        statex_idx = poppy_qm.X.GetFlatIdx(statex_d)
        statey_idx = poppy_qm.Y.GetFlatIdx(statey_d)

        # uix = self.quasix.GetDrawnPolicy((xpos, xvel))
        # utx = poppy_qm.Ux.Continuize(uix)

        # uiy = self.quasiy.GetDrawnPolicy((ypos, yvel))
        # uty = poppy_qm.Uy.Continuize(uiy)

        ugx = self.quasix.max_policy[statex_idx]
        ugy = self.quasiy.max_policy[statey_idx]

        if ugx is None:
            print 'X None'
            ugx = 5
        if ugy is None:
            print 'Y None'
            ugy = 5

        ux_idx = poppy_qm.Ux.GetIdxFromFlat(ugx)
        uy_idx = poppy_qm.Uy.GetIdxFromFlat(ugy)

        utx = poppy_qm.Ux.Continuize(ux_idx)
        uty = poppy_qm.Uy.Continuize(uy_idx)

        print 'GOAL', state, statex_d, statey_d, 'U', ux_idx, uy_idx, utx, uty
        # print 'GOAL', state, statex_d, statey_d, 'U', uix, uiy, utx, uty

        # utx = np.random.rand() * (poppy_qm.UXMAX - poppy_qm.UXMIN) + \
        #     poppy_qm.UXMIN
        # uty = np.random.rand() * (poppy_qm.UYMAX - poppy_qm.UYMIN) + \
        #     poppy_qm.UYMIN

        # print 'RAND:', utx, uty
        return (utx, uty)

    def move_up(self, leg):

        if leg == 'left':
            self.update()
            self.robot.lleg3d_up.add(
                0, 0, self.duration / 2.0)
            # self.robot.lleg3d_up.add(0, 0, 0.1)
            self.robot.lleg3d_up.start()
            self.robot.lleg3d_up.wait_to_start()
            self.robot.lleg3d_up.wait_to_stop()
            self.update()

            # s = '0 %f %f %f %f %f %f\n' % (self.lfoot_pos_com[0], self.lfoot_pos_com[1],
            # self.lfoot_pos_com[2], self.lfoot_vel_com[0],
            # self.lfoot_vel_com[1], self.lfoot_vel_com[2])

            # s = '0 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
            #                                                           self.lfoot_pos_com[
            #                                                               0], self.lfoot_pos_com[1],
            #                                                           self.lfoot_pos_com[
            #                                                               2], self.lfoot_vel_com[0],
            #                                                           self.lfoot_vel_com[
            #                                                               1], self.lfoot_vel_com[2],
            #                                                           self.rfoot_pos_com[
            #                                                               0], self.rfoot_pos_com[1],
            #                                                           self.rfoot_pos_com[
            #                                                               2], self.rfoot_vel_com[0],
            # self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            # self.fdyn_mid.write(s)

        elif leg == 'right':

            self.update()
            self.robot.rleg3d_up.add(0, 0, self.duration / 2.0)

            # self.robot.rleg3d_up.add(0, 0, 0.1)
            self.robot.rleg3d_up.start()
            self.robot.rleg3d_up.wait_to_start()
            self.robot.rleg3d_up.wait_to_stop()

            self.update()
            # print 'r vel', self.rfoot_vel_com

            # s = '1 %f %f %f %f %f %f\n' % (self.rfoot_pos_com[0], self.rfoot_pos_com[1],
            # self.rfoot_pos_com[2], self.rfoot_vel_com[0],
            # self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            # s = '1 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
            #                                                           self.lfoot_pos_com[
            #                                                               0], self.lfoot_pos_com[1],
            #                                                           self.lfoot_pos_com[
            #                                                               2], self.lfoot_vel_com[0],
            #                                                           self.lfoot_vel_com[
            #                                                               1], self.lfoot_vel_com[2],
            #                                                           self.rfoot_pos_com[
            #                                                               0], self.rfoot_pos_com[1],
            #                                                           self.rfoot_pos_com[
            #                                                               2], self.rfoot_vel_com[0],
            # self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            # self.fdyn_mid.write(s)

    def move_down(self, leg, x, y):

        if leg == 'left':

            self.robot.lleg3d_down.add(
                x, y, self.duration / 2.0)

            self.robot.lleg3d_down.start()
            self.robot.lleg3d_down.wait_to_start()
            self.robot.lleg3d_down.wait_to_stop()

        elif leg == 'right':

            self.robot.rleg3d_down.add(
                x, y, self.duration / 2.0)
            # self.robot.rleg3d_down.add( - 0.04 ,- 0.05 , self.duration / 2.0)
            self.robot.rleg3d_down.start()
            self.robot.rleg3d_down.wait_to_start()
            self.robot.rleg3d_down.wait_to_stop()

    def move(self, leg, x, y):

        if leg == 'left':
            self.update()
            self.robot.lleg3d_up.add(
                0, 0, self.duration / 2.0)
            # self.robot.lleg3d_up.add(0, 0, 0.1)
            self.robot.lleg3d_up.start()
            self.robot.lleg3d_up.wait_to_start()
            self.robot.lleg3d_up.wait_to_stop()
            self.update()

            # s = '0 %f %f %f %f %f %f\n' % (self.lfoot_pos_com[0], self.lfoot_pos_com[1],
            # self.lfoot_pos_com[2], self.lfoot_vel_com[0],
            # self.lfoot_vel_com[1], self.lfoot_vel_com[2])

            s = '0 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                                                                      self.lfoot_pos_com[
                                                                          0], self.lfoot_pos_com[1],
                                                                      self.lfoot_pos_com[
                                                                          2], self.lfoot_vel_com[0],
                                                                      self.lfoot_vel_com[
                                                                          1], self.lfoot_vel_com[2],
                                                                      self.rfoot_pos_com[
                                                                          0], self.rfoot_pos_com[1],
                                                                      self.rfoot_pos_com[
                                                                          2], self.rfoot_vel_com[0],
                                                                      self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            self.fdyn_mid.write(s)

            self.robot.lleg3d_down.add(
                x, y, self.duration / 2.0)

            self.robot.lleg3d_down.start()
            self.robot.lleg3d_down.wait_to_start()
            self.robot.lleg3d_down.wait_to_stop()

        elif leg == 'right':

            self.update()
            self.robot.rleg3d_up.add(0, 0, self.duration / 2.0)

            # self.robot.rleg3d_up.add(0, 0, 0.1)
            self.robot.rleg3d_up.start()
            self.robot.rleg3d_up.wait_to_start()
            self.robot.rleg3d_up.wait_to_stop()

            self.update()
            # print 'r vel', self.rfoot_vel_com

            # s = '1 %f %f %f %f %f %f\n' % (self.rfoot_pos_com[0], self.rfoot_pos_com[1],
            # self.rfoot_pos_com[2], self.rfoot_vel_com[0],
            # self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            s = '1 %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (self.goal_leg, self.goal_x, self.goal_y,
                                                                      self.lfoot_pos_com[
                                                                          0], self.lfoot_pos_com[1],
                                                                      self.lfoot_pos_com[
                                                                          2], self.lfoot_vel_com[0],
                                                                      self.lfoot_vel_com[
                                                                          1], self.lfoot_vel_com[2],
                                                                      self.rfoot_pos_com[
                                                                          0], self.rfoot_pos_com[1],
                                                                      self.rfoot_pos_com[
                                                                          2], self.rfoot_vel_com[0],
                                                                      self.rfoot_vel_com[1], self.rfoot_vel_com[2])

            self.fdyn_mid.write(s)

            self.robot.rleg3d_down.add(
                x, y, self.duration / 2.0)
            # self.robot.rleg3d_down.add( - 0.04 ,- 0.05 , self.duration / 2.0)
            self.robot.rleg3d_down.start()
            self.robot.rleg3d_down.wait_to_start()
            self.robot.rleg3d_down.wait_to_stop()


if __name__ == '__main__':

    Trx = poppy_qm.Transition(
        poppy_qm.X, poppy_qm.Ux, poppy_qm.XPosXVel_XPosXVelUx, None)
    Try = poppy_qm.Transition(
        poppy_qm.Y, poppy_qm.Uy, poppy_qm.YPosYVel_YPosYVelUy, None)

    # build the distribution: we don't need to do that for onlineqm as we experimentally learn the proba
    # Tr.ParallelBuild()

    print "Building graph"
    # quasi=OnlineQM(Tr,cost)
    quasix = poppy_qm.QM(Trx, poppy_qm.cost, LAMBDA=0.1)
    quasiy = poppy_qm.QM(Try, poppy_qm.cost, LAMBDA=0.1)

    # quasi.beta = 0.001 #more flat softmin=more exploration

    quasix.Init()
    quasiy.Init()

    poppy_qm.online_from_file_ind(sys.argv[1], quasix, quasiy)

    xpos = poppy_qm.Xpos.Discretize(0.0)
    ypos = poppy_qm.Ypos.Discretize(0.05)
    xvel = poppy_qm.Xvel.Discretize(0.0)
    yvel = poppy_qm.Yvel.Discretize(0.0)

    # goal = (xpos, ypos, xvel, yvel)
    # goal_vert = quasi.Vertices[ProbUtils.totuple(goal)]

    goalx = (xpos, xvel)
    goaly = (ypos, yvel)

    goal_vertx = quasix.Vertices[poppy_qm.ProbUtils.totuple(goalx)]
    goal_verty = quasiy.Vertices[poppy_qm.ProbUtils.totuple(goaly)]

    distx = quasix.Compute(goal_vertx)
    disty = quasiy.Compute(goal_verty)

    print distx.reshape((poppy_qm.XCARD, poppy_qm.DXCARD))
    print
    print disty.reshape((poppy_qm.YCARD, poppy_qm.DXCARD))

    # compute the policy for this goal
    # policy = quasi.ComputePolicy()
    # print policy

    policyx = quasix.ComputePolicy()
    print policyx.reshape((poppy_qm.XCARD, poppy_qm.DXCARD))
    print
    policyy = quasiy.ComputePolicy()
    print policyy.reshape((poppy_qm.YCARD, poppy_qm.DYCARD))

    poppy = from_vrep(poppy_config, '127.0.0.1', 19997, 'poppy-standing-hightorque.ttt',
                      tracked_collisions=['CollisionRFoot', 'CollisionLFoot'])

    # poppy = from_vrep(poppy_config, '127.0.0.1', 19997, vrep_scene=None, tracked_collisions=[
    #                   'CollisionRFoot', 'CollisionLFoot'])

    # time.sleep(10)
    client_id = poppy._controllers[0].io.client_id
    print 'ID', client_id

    # thread safe stuff

    vrepio = poppy._controllers[0].io

    obj_h = {}

    while obj_h == {}:

        objects = vrepio.call_remote_api('simxGetObjectGroupData',
                                         vrep.sim_object_shape_type, 0)

        obj_h = dict(zip(objects[3], objects[0]))
    print 'OBJ:', obj_h

    joint_h = {}
    while joint_h == {}:

        joints = vrepio.call_remote_api('simxGetObjectGroupData',
                                        vrep.sim_object_joint_type, 0)

        joint_h = dict(zip(joints[3], joints[0]))
    print 'JOINT:', joint_h

    dummies_h = {}
    while dummies_h == {}:

        dummies = vrepio.call_remote_api('simxGetObjectGroupData',
                                         vrep.sim_object_dummy_type, 0)

        dummies_h = dict(zip(dummies[3], dummies[0]))
    print 'DUMMY:', dummies_h

    vrepio.start_simulation()

    poppy.start_sync()

    poppy._primitive_manager._filter = np.sum

    # imu = vrep_tools.VrepIMU(client_id, obj_h['head_visual'])
    # imu.start()

    # poppy.attach_primitive(basic.StandPosition(poppy), 'stand_position')
    # poppy.stand_position.start()
    # poppy.stand_position.wait_to_start()

    # poppy.stand_position.wait_to_stop()

    print 'GO'
    dyn = Stab(poppy, client_id, obj_h, dummies_h, quasix, quasiy)

    dyn.run()
    # while True:
    #     dyn.update()
    #     print dyn.lfoot, dyn.rfoot, dyn.com, dyn.lfoot_pos_com, dyn.rfoot_pos_com
    #     time.sleep(0.1)
