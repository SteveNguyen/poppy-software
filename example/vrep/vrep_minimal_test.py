import sys
sys.path.append('/home/steve/divers/V-REP_PRO_EDU_V3_1_2_64_Linux/programming/remoteApiBindings/python/python/')
import time
import vrep


vrep.simxFinish(-1)
clientID=vrep.simxStart('127.0.0.1',19997,True,True,6000,5)

res, joint_h= vrep.simxGetObjectHandle(clientID, 'base_to_right_leg', vrep.simx_opmode_oneshot_wait)
print clientID, res, joint_h
#turn to positive angle
res = vrep.simxSetJointTargetPosition(clientID,joint_h,1.0,vrep.simx_opmode_oneshot)

print res
time.sleep(3)

vrep.simxFinish(clientID)
