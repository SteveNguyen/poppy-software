import numpy

from collections import deque

import pypot.primitive


class SmartCompliance(pypot.primitive.LoopPrimitive):

    def __init__(self, poppy_robot, motor_list, freq=50, ):
        pypot.primitive.LoopPrimitive.__init__(self, poppy_robot, freq)

        self.poppy_robot = poppy_robot
        self.motor_list = [self.get_mockup_motor(m) for m in motor_list]

        self.compute_angle_limit()

    def update(self):
        for i, m in enumerate(self.motor_list):
            angle_limit = self.angles[i]
            if (min(angle_limit) > m.present_position) or (m.present_position > max(angle_limit)):
                m.compliant = False
            else:
                m.compliant = True

    def compute_angle_limit(self):
        self.angles = []
        for m in self.motor_list:
            ang = numpy.asarray(
                m.angle_limit) if m.direct else -1 * numpy.asarray(m.angle_limit)
            ang = ang - m.offset
            self.angles.append(ang)


class ArmsCompliant(pypot.primitive.LoopPrimitive):

    def start(self):
        for m in self.robot.arms:
            m.compliant = True
        pypot.primitive.LoopPrimitive.start(self)

    def update(self):
        pass


class ArmsCopyMotion(pypot.primitive.LoopPrimitive):

    def start(self):
        for m in self.robot.arms:
            m.moving_speed = 0

        for m in self.robot.l_arm:
            m.compliant = True
        for m in self.robot.r_arm:
            m.compliant = False

        pypot.primitive.LoopPrimitive.start(self)

    def update(self):
        for lm, rm in zip(self.robot.l_arm, self.robot.r_arm):
            rm.goal_position = lm.present_position * (1 if lm.direct else -1)


class ArmsTurnCompliant(pypot.primitive.LoopPrimitive):

    def __init__(self, poppy_robot, freq):
        pypot.primitive.LoopPrimitive.__init__(self, poppy_robot, freq)

        self.poppy_robot = poppy_robot

        for m in self.poppy_robot.arms:
            m.compliant = False

        for m in self.poppy_robot.arms:
            m.torque_limit = 20

        self.left_arm_torque = deque([0], 0.2 * freq)
        self.right_arm_torque = deque([0], 0.2 * freq)

    def update(self):
        self.left_arm_torque.append(
            numpy.max([abs(m.present_load) for m in self.poppy_robot.l_arm]))
        self.right_arm_torque.append(
            numpy.max([abs(m.present_load) for m in self.poppy_robot.r_arm]))

        if numpy.mean(self.left_arm_torque) > 20:
            for m in self.poppy_robot.l_arm:
                m.compliant = True

        elif numpy.mean(self.left_arm_torque) < 7:
            for m in self.poppy_robot.l_arm:
                m.compliant = False

        if numpy.mean(self.right_arm_torque) > 20:
            for m in self.poppy_robot.r_arm:
                m.compliant = True

        elif numpy.mean(self.right_arm_torque) < 7:
            for m in self.poppy_robot.r_arm:
                m.compliant = False


class ArmsSmarterTurnCompliant(pypot.primitive.LoopPrimitive):

    def __init__(self, poppy_robot, freq):
        pypot.primitive.LoopPrimitive.__init__(self, poppy_robot, freq)

        self.poppy_robot = poppy_robot

        self.thres = 35
        self.timer = 10

        for m in self.poppy_robot.arms:
            m.compliant = False

        for m in self.poppy_robot.arms:
            m.torque_limit = self.thres

        self.left_arm_torque = deque([0], 0.2 * freq)
        self.right_arm_torque = deque([0], 0.2 * freq)

        self.l_arm_rec = []
        self.r_arm_rec = []

        self.rec_on_l = False
        self.rec_on_r = False
        self.play_it_l = 0
        self.play_it_r = 0

        self.l_timer = self.timer
        self.r_timer = self.timer

    def rec(self, arm):

        if arm == 'left':

            if not self.rec_on_l:
                self.rec_on_l = True
                self.l_arm_rec = []
                self.play_it_l = 0

            l = {}
            for m in self.poppy_robot.l_arm:
                l[m.name] = m.present_position
                # print m.name
            self.l_arm_rec.append(l)

        elif arm == 'right':
            if not self.rec_on_r:
                self.rec_on_r = True
                self.r_arm_rec = []
                self.play_it_r = 0

            r = {}
            for m in self.poppy_robot.r_arm:
                r[m.name] = m.present_position
            self.r_arm_rec.append(r)

    def play(self, arm):

        if arm == 'left' and len(self.l_arm_rec) > 0:
            self.rec_on_l = False
            l = self.l_arm_rec[self.play_it_l % len(self.l_arm_rec)]

            for k, d in l.iteritems():
                m = getattr(self.poppy_robot, k)
                m.goal_position = d

            self.play_it_l += 1
            # print self.play_it_l, len(self.l_arm_rec)

        elif arm == 'right' and len(self.r_arm_rec) > 0:
            self.rec_on_r = False
            r = self.r_arm_rec[self.play_it_r % len(self.r_arm_rec)]

            for k, d in r.iteritems():
                m = getattr(self.poppy_robot, k)
                m.goal_position = d

            self.play_it_r += 1

    def update(self):
        self.left_arm_torque.append(
            numpy.max([abs(m.present_load) for m in self.poppy_robot.l_arm]))
        self.right_arm_torque.append(
            numpy.max([abs(m.present_load) for m in self.poppy_robot.r_arm]))

        if numpy.mean(self.left_arm_torque) > self.thres:
            for m in self.poppy_robot.l_arm:
                m.compliant = True
            self.rec('left')
            # print 'rec left'
            self.l_timer = self.timer
        # elif numpy.mean(self.left_arm_torque) < self.thres - 10:
        # else:
        elif self.l_timer > 0:
            for m in self.poppy_robot.l_arm:
                m.compliant = True
                self.rec('left')
            self.l_timer -= 1

        else:
            for m in self.poppy_robot.l_arm:
                m.compliant = False
            self.play('left')
            # print 'play left'
        # else:
        #     self.l_timer -= 1

        if numpy.mean(self.right_arm_torque) > self.thres:
            for m in self.poppy_robot.r_arm:
                m.compliant = True
            self.rec('right')
            # print 'rec right'
            self.r_timer = self.timer
        # elif numpy.mean(self.right_arm_torque) < self.thres - 10:
        elif self.r_timer > 0:
            for m in self.poppy_robot.r_arm:
                m.compliant = True
                self.rec('right')
            self.r_timer -= 1

        else:

            # elif self.r_timer == 0:
            for m in self.poppy_robot.r_arm:
                m.compliant = False
            self.play('right')
            # print 'play right'

        # else:
        #     self.r_timer -= 1


class ArmsSmartestTurnCompliant(pypot.primitive.LoopPrimitive):

    def __init__(self, poppy_robot, freq):
        pypot.primitive.LoopPrimitive.__init__(self, poppy_robot, freq)

        self.poppy_robot = poppy_robot

        self.thres = 35
        self.timer = 30
        self.err_thres = 1

        for m in self.poppy_robot.arms:
            m.compliant = False

        for m in self.poppy_robot.arms:
            m.torque_limit = 70
            # print m.pid
            m.pid = (4.0, 0, 0)

        # self.left_arm_torque = deque([0], 0.2 * freq)
        # self.right_arm_torque = deque([0], 0.2 * freq)

        self.left_arm_err = deque([0], 0.2 * freq)
        self.right_arm_err = deque([0], 0.2 * freq)

        self.l_arm_rec = []
        self.r_arm_rec = []

        self.rec_on_l = False
        self.rec_on_r = False
        self.play_it_l = 0
        self.play_it_r = 0

        self.l_timer = 0
        self.r_timer = 0

    def get_err(self, arm):

        err = []
        if arm == 'left':
            for m in self.poppy_robot.l_arm:
                err.append(abs(m.present_position - m.goal_position))

        elif arm == 'right':
            for m in self.poppy_robot.r_arm:
                err.append(abs(m.present_position - m.goal_position))

        return err

    def rec(self, arm):

        if arm == 'left':

            if not self.rec_on_l:
                self.rec_on_l = True
                self.l_arm_rec = []
                self.play_it_l = 0

            l = {}
            for m in self.poppy_robot.l_arm:
                l[m.name] = m.present_position
                # print m.name
            self.l_arm_rec.append(l)

        elif arm == 'right':
            if not self.rec_on_r:
                self.rec_on_r = True
                self.r_arm_rec = []
                self.play_it_r = 0

            r = {}
            for m in self.poppy_robot.r_arm:
                r[m.name] = m.present_position
            self.r_arm_rec.append(r)

    def play(self, arm):

        if arm == 'left' and len(self.l_arm_rec) > 0:
            self.rec_on_l = False
            l = self.l_arm_rec[self.play_it_l % len(self.l_arm_rec)]

            for k, d in l.iteritems():
                m = getattr(self.poppy_robot, k)
                m.goal_position = d

            self.play_it_l += 1
            # print self.play_it_l, len(self.l_arm_rec)

        elif arm == 'right' and len(self.r_arm_rec) > 0:
            self.rec_on_r = False
            r = self.r_arm_rec[self.play_it_r % len(self.r_arm_rec)]

            for k, d in r.iteritems():
                m = getattr(self.poppy_robot, k)
                m.goal_position = d

            self.play_it_r += 1

    def update(self):

        self.left_arm_err.append(
            numpy.max(self.get_err('left')))

        self.right_arm_err.append(
            numpy.max(self.get_err('right')))

        if numpy.mean(self.right_arm_err) > self.err_thres:

            for m in self.poppy_robot.r_arm:
                # m.compliant = True
                m.goal_position = m.present_position
            self.rec('right')
            self.r_timer = self.timer
            print 'rec'

        elif self.r_timer > 0:
            for m in self.poppy_robot.r_arm:
                m.goal_position = m.present_position

            self.rec('right')
            self.r_timer -= 1
            print 'rec else'

        else:

            self.play('right')
            print 'play'

        # self.left_arm_torque.append(
        #     numpy.max([abs(m.present_load) for m in self.poppy_robot.l_arm]))
        # self.right_arm_torque.append(
        # numpy.max([abs(m.present_load) for m in
        # self.poppy_robot.r_arm]))

        # if numpy.mean(self.left_arm_torque) > self.thres:
        #     for m in self.poppy_robot.l_arm:
        #         m.compliant = True
        #     self.rec('left')
        # print 'rec left'
        #     self.l_timer = self.timer
        # elif numpy.mean(self.left_arm_torque) < self.thres - 10:
        # else:
        # elif self.l_timer > 0:
        #     for m in self.poppy_robot.l_arm:
        #         m.compliant = True
        #         self.rec('left')
        #     self.l_timer -= 1

        # else:
        #     for m in self.poppy_robot.l_arm:
        #         m.compliant = False
        #     self.play('left')
        # print 'play left'
        # else:
        # self.l_timer -= 1

        # if numpy.mean(self.right_arm_torque) > self.thres:
        #     for m in self.poppy_robot.r_arm:
        #         m.compliant = True
        #     self.rec('right')
        # print 'rec right'
        #     self.r_timer = self.timer
        # elif numpy.mean(self.right_arm_torque) < self.thres - 10:
        # elif self.r_timer > 0:
        #     for m in self.poppy_robot.r_arm:
        #         m.compliant = True
        #         self.rec('right')
        #     self.r_timer -= 1

        # else:

        # elif self.r_timer == 0:
        #     for m in self.poppy_robot.r_arm:
        #         m.compliant = False
        #     self.play('right')
        # print 'play right'

        # else:
        # self.r_timer -= 1
