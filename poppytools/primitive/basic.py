import time
import itertools

import pypot.primitive

class TurnArmsOn(pypot.primitive.Primitive):
    def setup(self):
        print("Arms power up")

        for m in self.robot.arms:
            m.torque_limit = 100

        time.sleep(0.5)

class TurnArmsNormal(pypot.primitive.Primitive):
    def setup(self):
        print("Arms power normal")

        for m in self.robot.arms:
            m.torque_limit = 70

        time.sleep(0.5)



class TempSecurity(pypot.primitive.LoopPrimitive):

    def __init__(self, robot, limit=70, refresh_freq = 0.2):
        pypot.primitive.LoopPrimitive.__init__(self, robot,refresh_freq)
        self.limit = limit

    def update(self):
        t = {}
        for m in self.robot.motors:
            t[m.name] = m.present_temperature
            if m.present_temperature > self.limit:
                print "WARNING: temperature limit!", m.name, m.present_temperature
                m.torque_max = 0
                m.compliant = True
        print t

class InitRobot(pypot.primitive.Primitive):
    def setup(self):
        print("initialisation")

        self.robot.compliant = False

        self.robot.power_max()

        # Change PID of Dynamixel MX motors
        for m in filter(lambda m: hasattr(m, 'pid'), self.robot.motors):
            m.pid = (4, 2, 0)

        # Reduce max torque to keep motor temperature low
        for m in self.robot.motors:
            m.torque_limit = 70

        for m in self.robot.torso:
            m.pid = (6, 2, 0)

        time.sleep(0.5)


class StandPosition(InitRobot):

    def run(self):
        # Goto to position 0 on all motors
        self.robot.goto_position(dict(zip((m.name for m in self.robot.motors),
                                            itertools.repeat(0))),
                                            2)

        # Specified some motor positions to keep the robot balanced
        self.robot.goto_position({'r_hip_z': -2,
                                'l_hip_z': 2,
                                'r_hip_x': -2,
                                'l_hip_x': 2,
                                'l_shoulder_x': 10,
                                'r_shoulder_x': -10,
                                'l_shoulder_y': 10,
                                'r_shoulder_y': 10,
                                'l_elbow_y': -20,
                                'r_elbow_y': -20,
                                'l_ankle_y': -3,
                                'r_ankle_y': -3,
                                'abs_y': -3,
                                'head_y': 0,
                                'head_z':0},
                                3,
                                wait=True)

        # Restore the motor speed
        self.robot.power_max()

        # Reduce max torque to keep motor temperature low
        for m in self.robot.motors:
            m.torque_limit = 70


        time.sleep(0.5)


class SitPosition(pypot.primitive.Primitive):
    def run(self):
        self.robot.l_hip_y.goal_position = -35
        self.robot.r_hip_y.goal_position = -35
        self.robot.l_knee_y.goto_position(125, 2)
        self.robot.r_knee_y.goto_position(125, 2, wait=True)

        for m in self.robot.torso:
            m.goal_position = 0

        self.robot.abs_y.goal_position = 0

        motor_list = [self.robot.l_knee_y, self.robot.l_ankle_y, self.robot.r_knee_y, self.robot.r_ankle_y]

        self.robot.l_hip_x.torque_limit = 20
        self.robot.r_hip_x.torque_limit = 20
        self.robot.l_hip_z.torque_limit = 20
        self.robot.r_hip_z.torque_limit = 20
        self.robot.bust_y.torque_limit = 20


        # motor_list = [self.robot.l_knee_y, self.robot.l_ankle_y, self.robot.r_knee_y, self.robot.r_ankle_y, self.robot.l_hip_x, self.robot.r_hip_x]


        for m in motor_list:
            m.compliant = True


        self.robot.head_y.torque_limit = 100
        self.robot.head_z.torque_limit = 100


        time.sleep(2)
