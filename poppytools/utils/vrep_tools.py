#!/usr/bin/python
# -*- coding: utf-8 -*-

########################################################################
#  File Name	: 'vrep_tools.py'
#  Author	: Steve NGUYEN
#  Contact      : steve.nguyen@inria.fr
#  Created	: Wednesday, July  9 2014
#  Revised	:
#  Version	:
#  Target MCU	:
#
#  This code is distributed under the GNU Public License
# 		which can be found at http://www.gnu.org/licenses/gpl.txt
#
#
#  Notes:	notes
########################################################################

import time

import pypot.vrep
# import vrep
import pypot.vrep.remoteApiBindings.vrep as vrep
import math
import threading

import poppytools.sensor.inertial_unit as inertial_unit


vrep_error = {'ok': vrep.simx_return_ok,
              'novalue': vrep.simx_return_novalue_flag,
              'timeout': vrep.simx_return_timeout_flag,
              'opmode': vrep.simx_return_illegal_opmode_flag,
              'remote': vrep.simx_return_remote_error_flag,
              'progress': vrep.simx_return_split_progress_flag,
              'local': vrep.simx_return_local_error_flag,
              'init': vrep.simx_return_initialize_error_flag}


def check_error(err):
    for e, c in vrep_error.iteritems():
        if err == c:
            return e


def get_client_id(robot):
    return robot._controllers[0].io.client_id


class VrepIMU(threading.Thread):

    def __init__(self, client_id, head_handler):
        threading.Thread.__init__(self)

        self.daemon = True
        self.client_id = client_id
        self.head_handler = head_handler
        self.acc = inertial_unit.Vector()
        self.tilt = inertial_unit.Vector()
        self.gyro = inertial_unit.Vector()

        self.err = ''  # fixme
        self.init()

    def init(self):

        ace = ''
        gye = ''
        while ace != 'ok' or gye != 'ok':

            head_pos = vrep.simxGetObjectPosition(
                self.client_id, self.head_handler, - 1, vrep.simx_opmode_streaming)
            # head_vel = vrep.simxGetObjectVelocity(self.client_id, self.head_handler,vrep.simx_opmode_streaming)
            head_ori = vrep.simxGetObjectOrientation(
                self.client_id, self.head_handler, - 1, vrep.simx_opmode_streaming)

            acc_x = vrep.simxGetFloatSignal(
                self.client_id, "HeadAccelerometerX", vrep.simx_opmode_streaming)
            acc_y = vrep.simxGetFloatSignal(
                self.client_id, "HeadAccelerometerY", vrep.simx_opmode_streaming)
            acc_z = vrep.simxGetFloatSignal(
                self.client_id, "HeadAccelerometerZ", vrep.simx_opmode_streaming)

            gyr_x = vrep.simxGetFloatSignal(
                self.client_id, "HeadGyroX", vrep.simx_opmode_streaming)
            gyr_y = vrep.simxGetFloatSignal(
                self.client_id, "HeadGyroY", vrep.simx_opmode_streaming)
            gyr_z = vrep.simxGetFloatSignal(
                self.client_id, "HeadGyroZ", vrep.simx_opmode_streaming)

            ace = check_error(acc_x[0])
            gye = check_error(gyr_x[0])
            # print gye
        # self.prev_x = math.degrees(head_vel[1][0])
        # self.prev_y = math.degrees(head_vel[1][1])
        # self.prev_z = math.degrees(head_vel[1][2])

    def run(self):
        while True:

            head_pos = vrep.simxGetObjectPosition(
                self.client_id, self.head_handler, - 1, vrep.simx_opmode_buffer)
            # head_vel = vrep.simxGetObjectVelocity(self.client_id, self.head_handler,vrep.simx_opmode_buffer)
            head_ori = vrep.simxGetObjectOrientation(
                self.client_id, self.head_handler, - 1, vrep.simx_opmode_buffer)

            acc_x = vrep.simxGetFloatSignal(
                self.client_id, "HeadAccelerometerX", vrep.simx_opmode_buffer)
            acc_y = vrep.simxGetFloatSignal(
                self.client_id, "HeadAccelerometerY", vrep.simx_opmode_buffer)
            acc_z = vrep.simxGetFloatSignal(
                self.client_id, "HeadAccelerometerZ", vrep.simx_opmode_buffer)

            gyr_x = vrep.simxGetFloatSignal(
                self.client_id, "HeadGyroX", vrep.simx_opmode_buffer)
            gyr_y = vrep.simxGetFloatSignal(
                self.client_id, "HeadGyroY", vrep.simx_opmode_buffer)
            gyr_z = vrep.simxGetFloatSignal(
                self.client_id, "HeadGyroZ", vrep.simx_opmode_buffer)

            # print acc_x, acc_y, acc_z

            self.err = check_error(acc_x[0])

            # with head visual #FIXME
            self.tilt.x = - math.degrees(head_ori[1][1])
            self.tilt.y = math.degrees(head_ori[1][0])
            self.tilt.z = math.degrees(head_ori[1][2]) - 90.0

            # print self.tilt.x, self.tilt.y, self.tilt.z

            self.gyro.x = gyr_x[1]
            self.gyro.y = gyr_y[1]
            self.gyro.z = gyr_z[1]

            # self.gyro.x = math.degrees(head_vel[2][0])
            # self.gyro.y = math.degrees(head_vel[2][1])
            # self.gyro.z = math.degrees(head_vel[2][2])

            # self.acc_x = (math.degrees(head_vel[1][0]) - self.prev_x) / 0.001
            # self.acc_y = (math.degrees(head_vel[1][1]) - self.prev_y) / 0.001
            # self.acc_z = (math.degrees(head_vel[1][2]) - self.prev_z) / 0.001

            self.acc.x = acc_x[1]
            self.acc.y = acc_y[1]
            self.acc.z = acc_z[1]

            # self.prev_x = math.degrees(head_vel[1][0])
            # self.prev_y = math.degrees(head_vel[1][1])
            # self.prev_z = math.degrees(head_vel[1][2])

            time.sleep(0.001)
